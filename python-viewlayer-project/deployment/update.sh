#!/bin/bash

export BUILD_VERSION=$(cat package-store/build_version)
mkdir -p ~/.ssh
ls ~/.ssh
echo "$KEY_STRING" > ~/.ssh/server_key
chmod 700 ~/.ssh
chmod 600 ~/.ssh/server_key
echo -e "Host $SERVER_IP\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
ls ~/.ssh
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "/home/$KEY_USER/run/target.sh $BUILD_VERSION build"
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "/home/$KEY_USER/run/current.sh down --rmi local"
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "ln -sfT /home/$KEY_USER/webserver/$BUILD_VERSION /home/$KEY_USER/webserver/current"
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "/home/$KEY_USER/run/current.sh run web python /code/server/manage.py collectstatic --no-input"
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "/home/$KEY_USER/run/current.sh run web python /code/server/manage.py migrate --no-input"
ssh -i ~/.ssh/server_key $KEY_USER@$SERVER_IP "/home/$KEY_USER/run/current.sh up -d"

#!/bin/bash

export BUILD_VERSION=$(cat package-store/build_version)
mkdir deploy-ready-files/styleguide
tar -C deploy-ready-files/styleguide -xvzf package-store/styleguide.tar.gz
mkdir deploy-ready-files/webserver
mkdir deploy-ready-files/webserver/$BUILD_VERSION
mkdir deploy-ready-files/webserver/$BUILD_VERSION/viewlayer
mkdir deploy-ready-files/webserver/$BUILD_VERSION/server
tar -C deploy-ready-files/webserver/$BUILD_VERSION -xzf package-store/deployment.tar.gz
tar -C deploy-ready-files/webserver/$BUILD_VERSION/server -xzf package-store/server.tar.gz
tar -C deploy-ready-files/webserver/$BUILD_VERSION/viewlayer -xzf package-store/viewlayer.tar.gz
ls ./deploy-ready-files

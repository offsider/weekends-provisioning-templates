
<% if (componentFolder == "views") { %>import InitClient from '../../../InitClient'

export default function (container: Element) {
  InitClient(container as HTMLElement)
}

<% } else { %>import client from './<%= componentName %>.client'

export default function (container: Element) {
  client(container)
}
<% } %>

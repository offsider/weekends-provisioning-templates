
import * as React from 'react'
import * as DOM from 'react-dom-factories'
import {bind} from '../../../platform/Utils'
import {ContextProps, withAppContext} from '../../../platform/AppContext'
import bem from '../../../platform/Utils/bem'
<% if (componentFolder == "views") { %>
import { <%= componentName %>ViewData } from "../../../cms-data/<%= componentName %>Data"
import nl2br from "../../../platform/Utils/nl2br"
import { flexContentRender } from "../../Foundation/Richtext"

// Components
import BaseLayout from "../../layouts/BaseLayout"
import Imgset from "../../../platform/UI/Imgset"
<% } %>

<% if (typedProps) { %>
<% if (componentFolder == "views") { %>
export interface <%= componentName %>Props extends ContextProps, <%= componentName %>ViewData {}
<% } else { %>
export interface <%= componentName %>Props extends ContextProps {}
<% } %>
<% } %>
<% if (typedState) { %>
interface <%= componentName %>State {
}
<% } %>

@withAppContext
export default class <%= componentName %> extends React.PureComponent<<%= typedProps ? componentName + 'Props' : '{}' %>, <%= typedState ? componentName + 'State' : '{}' %>>
{
  static displayName = '<%= componentName %>'

  <% if (componentFolder == "views") { %>
  render () {
    const cx = bem('<%= componentName %>')
    return (
      <BaseLayout {...this.props}>
        <div className={cx('&')}>
          <div className={cx('&__body')}>
            {this.props.children}
          </div>
        </div>
      </BaseLayout>
    )
  }
  <% } else { %>
  render () {
    const cx = bem('<%= componentName %>')
    return <div className={cx('&')}>
      <div className={cx('&__body')}>
        {this.props.children}
      </div>
    </div>
  }
  <% } %>
}

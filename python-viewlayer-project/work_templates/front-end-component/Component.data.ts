import {<%= componentName %>Props} from './<%= componentName %>.react'
<% if (componentFolder == "views") { %>
import { <%= componentName %>ViewContent } from "../../../cms-data/<%= componentName %>Data"

export const standard: <%= componentName %>Props = <%= componentName %>ViewContent
<% } else { %>
export const standard: <%= componentName %>Props = {
}
<% } %>

var fs = require('fs')

module.exports.questions = function () {
  const dirs = fs.readdirSync(__dirname + '/../../front-end/ui')

  return [{
    "type": "input",
    "name": "componentName",
    "message": "What is the name of your component?",
  }, {
    "type": "list",
    "name": "componentFolder",
    "choices": dirs,
    "message": "What component folder should this go in?",
  }, {
    "type": "confirm",
    "name": "typedProps",
    "message": "Use typed properties",
  }, {
    "type": "confirm",
    "name": "typedState",
    "message": "Use typed state",
  }, {
    "type": "confirm",
    "name": "clientScript",
    "message": "Create client script",
  }, {
    "type": "confirm",
    "name": "testScript",
    "message": "Create test script",
  }]
}

module.exports.setup = function (projectRoot, answers) {
  const target = (
    projectRoot + '/front-end/ui/' + answers.componentFolder + '/' +
    answers.componentName
  )

  const files = [{
    src: '_index.ts',
    data: answers,
    dest: target + '/index.ts',
  }, {
    src: 'Component.react.tsx',
    data: answers,
    dest: target + '/' + answers.componentName + '.react.tsx',
  }, {
    src: 'Component.data.ts',
    data: answers,
    dest: target + '/' + answers.componentName + '.data.tsx',
  }, {
    src: 'Component.less',
    data: answers,
    dest: target + '/' + answers.componentName + '.less',
  }]

  if (answers.clientScript) {
    files.push({
      src: 'Component.client.ts',
      data: answers,
      dest: target + '/' + answers.componentName + '.client.ts',
    })
  }

  if (answers.testScript) {
    files.push({
      src: 'Component.test.ts',
      data: answers,
      dest: target + '/' + answers.componentName + '.test.ts',
    })
  }

  return files
}

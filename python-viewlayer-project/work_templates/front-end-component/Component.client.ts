<% if (componentFolder == "views") { %>
import Imgset from "../../../platform/UI/Imgset/Imgset.client"

export default function(scope: Element) {
  const view = scope.querySelector(".<%= componentName %>")
  if (!view) return () => {}

  Array.from(view.querySelectorAll(".Imgset")).forEach((img: HTMLElement) => {
    Imgset(img)
  })

  const unloadFns: (() => void)[] = []

  return () => {
    unloadFns.forEach((fn) => fn())
  }
}
<% } else { %>
export default function(scope: Element) {}
<% } %>

var anathema = require('./config')
var webpack = require('webpack')
var path = require('path')
var ReplacePlugin = require('webpack-plugin-replace')
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin
var nodeExternals = require('webpack-node-externals')


const scriptsMonitor = anathema.monitor("webpack")


const serverAppName = anathema.config.serverAppName
const projectName = anathema.config.projectName


function generateWebpackConfig (task) {
  const {src, staticOut, componentServerOut, viewLayerOut} = anathema.config.paths
  const PACKED = anathema.config.packed

  const bundleAnalyzerPlugin = new BundleAnalyzerPlugin({
    generateStatsFile: true
  })

  const serverDefinePlugin = anathema.config.packed
    ? new webpack.DefinePlugin({
        'process.env': {
          COMPONENT_MEDIA_SRC: "__dirname + '/../../../front-end/cms-media'"
        }
      })
    : new webpack.DefinePlugin({
      })

  const COMMON_CONFIG = {
    mode: PACKED ? "production" : "development",
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    module: {
      rules: [
        { test: /\.tsx?$/, loader: "ts-loader" },
      ]
    },
  }

  const CLIENT_COMMON_CONFIG = {
    externals: {
      'jsdom': 'false',
    },
    module: {
      rules: PACKED
        ? [
            { test: /\.tsx?$/, loader: "ts-loader" },
            {
              test: /\.js$/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['babel-preset-es2015']
                }
              }
            }
          ]
        : [
            { test: /\.tsx?$/, loader: "ts-loader" },
            // supporting IE10 requires the below rule, add for testing
            {
              test: /\.js$/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['babel-preset-es2015']
                }
              }
            }
          ]
    },
  }

  const SERVER_COMMON_CONFIG = {
    externals: [nodeExternals()],
    target: "node",
    node: {__dirname: false},
  }

  const WEBPACK_CONFIG = [
    Object.assign({}, COMMON_CONFIG, CLIENT_COMMON_CONFIG, {
      entry: anathema.rootDirectory + "/" + src + "/Main.tsx",
      node: {express: 'empty', fs: 'empty', net: 'empty'},
      output: {
        filename: projectName + ".pkg.js",
        path: anathema.rootDirectory + '/' + staticOut,
      },
    }),
    Object.assign({}, COMMON_CONFIG, SERVER_COMMON_CONFIG, {
      entry: anathema.rootDirectory + "/" + src + "/view-layer/server.js",
      output: {
        filename: projectName + "ViewLayer.server.js",
        path: anathema.rootDirectory + '/' + viewLayerOut,
      },
      plugins: [],
    }),
    Object.assign({}, COMMON_CONFIG, SERVER_COMMON_CONFIG, {
      entry: anathema.rootDirectory + "/" + src + "/component-server/server.ts",
      output: {
        filename: projectName + "Components.server.js",
        path: anathema.rootDirectory + '/' + componentServerOut,
      },
      plugins: [serverDefinePlugin],
    }),
    Object.assign({}, COMMON_CONFIG, CLIENT_COMMON_CONFIG, {
      entry: anathema.rootDirectory + "/" + src + "/component-server/client.ts",
      externals: [{
        'jsdom': 'false',
        'react-dom': 'false',
        'luxon': 'false',
        'i18next': 'false',
        'svgo': 'false',
        'on-demand-resizer': 'false',
        'react-dom': 'false',
        'react-dom/server': 'false',
        'express': 'false',
      }],
      node: {express: 'empty', fs: 'empty', net: 'empty'},
      output: {
        filename: projectName + "Components.client.js",
        path: anathema.rootDirectory + '/' + componentServerOut,
      },
      // plugins: [bundleAnalyzerPlugin],
    }),
  ]

  return WEBPACK_CONFIG
}



anathema.task("scripts:isolated", async function (task) {
  const {src, staticOut, componentServerOut, viewLayerOut} = anathema.config.paths
  const PACKED = anathema.config.packed
  const WEBPACK_CONFIG = generateWebpackConfig(task)
  await new Promise((resolve, reject) => {
    const compiler = webpack(WEBPACK_CONFIG[0])
    compiler.run((err, stats) => {
      console.log("Finished Compile: Standard Client")
      if (err) { return reject(err) }
      if (stats.hasErrors()) { return reject(stats.toString({ all: false, errors: true, colors: true, chunks: false })) }
      resolve(stats)
    })
  })
  await new Promise((resolve, reject) => {
    const compiler = webpack(WEBPACK_CONFIG[1])
    compiler.run((err, stats) => {
      console.log("Finished Compile: View Layer Server")
      if (err) { return reject(err) }
      if (stats.hasErrors()) { return reject(stats.toString({ all: false, errors: true, colors: true, chunks: false })) }
      resolve(stats)
    })
  })
  await new Promise((resolve, reject) => {
    const compiler = webpack(WEBPACK_CONFIG[2])
    compiler.run((err, stats) => {
      console.log("Finished Compile: Components Server")
      if (err) { return reject(err) }
      if (stats.hasErrors()) { return reject(stats.toString({ all: false, errors: true, colors: true, chunks: false })) }
      resolve(stats)
    })
  })
  await new Promise((resolve, reject) => {
    const compiler = webpack(WEBPACK_CONFIG[3])
    compiler.run((err, stats) => {
      console.log("Finished Compile: Components Client")
      if (err) { return reject(err) }
      if (stats.hasErrors()) { return reject(stats.toString({ all: false, errors: true, colors: true, chunks: false })) }
      resolve(stats)
    })
  })


  task.stats.filesOutput.push("/" + staticOut + "/" + projectName + ".pkg.js")
  task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.server.js")
  task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.client.js")

  return true
})

anathema.task("scripts", function (task) {
  const {src, staticOut, componentServerOut, viewLayerOut} = anathema.config.paths
  const PACKED = anathema.config.packed
  const WEBPACK_CONFIG = generateWebpackConfig(task)
  const compiler = webpack(WEBPACK_CONFIG)

  if (task.runContext.dashboard) {
    compiler.watch({}, (err, stats) => {
      if (err) {
        return scriptsMonitor.reportFailure(err)
      }

      if (stats.hasErrors()) {
        return scriptsMonitor.reportFailure(
          stats.toString({
            all: false, errors: true, colors: true, chunks: false
          })
        )
      }

      const start = Math.min(stats.stats.map((s) => s.startTime))
      const end = Math.max(stats.stats.map((s) => s.endTime))

      scriptsMonitor.reportSuccess(
        stats.toString({colors: true}),
        end - start
      )
    })
    task.stats.filesOutput.push("/" + staticOut + "/" + projectName + ".pkg.js")
    task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.server.js")
    task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.client.js")

    return Promise.resolve(true)
  } else {
    return new Promise((resolve, reject) => {
      compiler.run((err, stats) => {
        if (err) {
          return reject(err)
        }

        if (stats.hasErrors()) {
          return reject(stats.toString({
            all: false, errors: true, colors: true, chunks: false
          }))
        }

        // stats.compilation.fileDependencies.forEach((name) => {
        //   task.stats.filesMatched.push(name)
        // })
        task.stats.filesOutput.push("/" + staticOut + "/" + projectName + ".pkg.js")
        task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.server.js")
        task.stats.filesOutput.push("/" + componentServerOut + "/" + projectName + "Components.client.js")

        resolve(stats)
      })
    })
  }
})

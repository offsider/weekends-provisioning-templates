var { Anathema } = require("anathema")
let anathema = new Anathema()

anathema.config = {
  projectName: "Applecore",
  serverAppName: "applecore",
  paths: {
    src: "front-end",
    serverSrc: "server",
    staticOut: "build/assets",
    componentServerOut: "build/component-server",
    componentServerAssetsOut: "build/component-server/assets",
    viewLayerOut: "build/view-layer",
    buildRoot: "build",
    serverDist: "build/dist/server",
    viewLayerDist: "build/dist/viewlayer",
  },
  packed: false,
}

module.exports = anathema

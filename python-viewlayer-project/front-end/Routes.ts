import { RouteStack, addRoute, getUrl, ViewParams } from "./platform/Routing"

const viewLayerRoutingEn: RouteStack = { rootPath: "", routes: [] }
addRoute(viewLayerRoutingEn, "home", "/")
addRoute(viewLayerRoutingEn, "profile", "/profile/")
addRoute(viewLayerRoutingEn, "projects", "/projects/")
addRoute(viewLayerRoutingEn, "project", "/projects/:slug/")
addRoute(viewLayerRoutingEn, "stories", "/stories/")
addRoute(viewLayerRoutingEn, "story", "/stories/:slug/")
addRoute(viewLayerRoutingEn, "objects", "/objects/")
addRoute(viewLayerRoutingEn, "object", "/objects/:slug/")
addRoute(viewLayerRoutingEn, "openings", "/openings/")
addRoute(viewLayerRoutingEn, "contact", "/contact/")
// addRoute(viewLayerRoutingEn, "resources", "/resources/")
// addRoute(viewLayerRoutingEn, "faq", "/faq/")
// addRoute(viewLayerRoutingEn, "faq-individual", "/faq/:slug/")

const componentServerRouting: RouteStack = { rootPath: "", routes: [] }
addRoute(
  componentServerRouting,
  "home",
  "/views/HomePage/HomePage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "profile",
  "/views/ProfilePage/ProfilePage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "contact",
  "/views/ContactPage/ContactPage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "openings",
  "/views/CurrentOpenings/CurrentOpenings.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "projects",
  "/views/ProjectsListingPage/ProjectsListingPage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "project",
  "/views/ProjectPage/ProjectPage.react.tsx/:slug"
)
addRoute(
  componentServerRouting,
  "objects",
  "/views/ObjectsListingPage/ObjectsListingPage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "object",
  "/views/ObjectPage/ObjectPage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "stories",
  "/views/StoriesListingPage/StoriesListingPage.react.tsx/standard"
)
addRoute(
  componentServerRouting,
  "story",
  "/views/StoryPage/StoryPage.react.tsx/standard"
)
// addRoute(
//   componentServerRouting,
//   "faq",
//   "/views/FAQHomePage/FAQHomePage.react.tsx/standard"
// )
// addRoute(
//   componentServerRouting,
//   "locality-root",
//   "/views/LocalityReportPage/LocalityReportPage.react.tsx/:locality"
// )

export const viewLayerGetUrlEn = (name: string, params?: ViewParams) => {
  return getUrl(viewLayerRoutingEn, name, params)
}

export const componentServerGetUrl = (name: string, params?: ViewParams) => {
  return getUrl(componentServerRouting, name, params)
}

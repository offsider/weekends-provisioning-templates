import HomePage from "./ui/views/HomePage/HomePage.client"
import GenericPage from "./ui/views/GenericPage/GenericPage.client"

import BaseLayout from "./ui/layouts/BaseLayout/BaseLayout.client"

export default function(container: HTMLElement) {
  const initFunc = (scope: HTMLElement) => {
    if (!scope) return

    const unloadHomePage = HomePage(scope)
    const unloadGenericPage = GenericPage(scope)

    return () => {}
  }

  // initFunc(container.querySelector("body"))
  initFunc(container)
  BaseLayout(container)
}

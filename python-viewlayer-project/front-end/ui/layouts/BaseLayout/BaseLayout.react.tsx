import * as React from "react"
import * as DOM from "react-dom-factories"
import { bind } from "../../../platform/Utils"
import { ContextProps, withAppContext } from "../../../platform/AppContext"
import bem from "../../../platform/Utils/bem"
import { GlobalData } from "../../../cms-data/GlobalData"

// import SiteHeader from "../../molecules/SiteHeader"

export interface BaseLayoutProps extends ContextProps, GlobalData {
  // backgroundColor?: "primary" | "white"
  // showFooter?: boolean
}

@withAppContext
export default class BaseLayout extends React.PureComponent<
  BaseLayoutProps,
  {}
> {
  static displayName = "BaseLayout"

  render() {
    const cx = bem("BaseLayout")
    const classes = {
      "&": true,
    }
    return (
      <>
        <header>header...</header>
        {this.props.children}
      </>
    )
  }
}

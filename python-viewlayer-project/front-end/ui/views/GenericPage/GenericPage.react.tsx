import * as React from "react"
import * as DOM from "react-dom-factories"
import { bind } from "../../../platform/Utils"
import { ContextProps, withAppContext } from "../../../platform/AppContext"
import { GenericPageViewData } from "../../../cms-data/GenericPageData"
import bem from "../../../platform/Utils/bem"
import BaseLayout from "../../layouts/BaseLayout"
import { inlineRenderMd, flexContentRender } from "../../Foundation/Richtext"

export interface GenericPageProps extends ContextProps, GenericPageViewData {}

@withAppContext
export default class GenericPage extends React.PureComponent<
  GenericPageProps,
  {}
> {
  static displayName = "GenericPage"

  render() {
    const cx = bem("GenericPage")
    // const bodyContent = flexContentRender(this.props.content.body)

    return (
      <BaseLayout {...this.props}>
        <div className={cx("&")}>
          <div className={cx("&__body")}>
            <h1 className={cx("&__title")}>
              {inlineRenderMd(this.props.content.title)}
            </h1>
            <div>{"testing autodeploy up in here"}</div>
          </div>
        </div>
      </BaseLayout>
    )
  }
}

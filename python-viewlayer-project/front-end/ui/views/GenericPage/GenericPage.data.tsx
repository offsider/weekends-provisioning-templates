import { GenericPageViewContent } from "../../../cms-data/GenericPageData"
import { GenericPageProps } from "./GenericPage.react"

export const standard: GenericPageProps = GenericPageViewContent

import Imgset from "../../../platform/UI/Imgset/Imgset.client"

export default function(scope: Element) {
  const view = scope.querySelector(".GenericPage")

  if (!view) return () => {}

  Array.prototype.forEach.call(view.querySelectorAll(".Imgset"), (img: any) => {
    Imgset(img)
  })

  return () => {}
}

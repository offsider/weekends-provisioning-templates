import * as React from "react"
import * as DOM from "react-dom-factories"
import { bind } from "../../../platform/Utils"
import { ContextProps, withAppContext } from "../../../platform/AppContext"
import { HomePageViewData } from "../../../cms-data/HomePageData"

import bem from "../../../platform/Utils/bem"
import Imgset from "../../../platform/UI/Imgset"
import BaseLayout from "../../layouts/BaseLayout"

export interface HomePageProps extends ContextProps, HomePageViewData {}

@withAppContext
export default class HomePage extends React.PureComponent<HomePageProps, {}> {
  static displayName = "HomePage"

  render() {
    const cx = bem("HomePage")
    return (
      <BaseLayout {...this.props}>
        <div className={cx("&")}>
          <div className={cx("&__body")}>
            <div className="span-3"></div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div className="span-6"></div>
              <div className="span-6"></div>
            </div>
            <div className="span-7"></div>
            <div className={cx("&__richtext*")}>
              <h1>h1</h1>
              <h2>h2</h2>
              <h3>h3</h3>
              <h4>h4</h4>
              <h5>h5</h5>
              <h6>h6</h6>
              <p>
                We specialise in the <a href="#!">intersection of design</a> and
                development — where strategy and purpose meets execution and
                technology.
              </p>
              <blockquote>
                We specialise in the <a href="#!">intersection of design</a> and
                development — where strategy and purpose meets execution and
                technology.
              </blockquote>
              <h3>Lists</h3>
              <ol>
                <li>One</li>
                <li>Two</li>
                <li>Three</li>
                <li>
                  Nested ul
                  <ul>
                    <li>Two</li>
                    <li>Three</li>
                  </ul>
                </li>
                <li>
                  Nested ol
                  <ol>
                    <li>Two</li>
                    <li>Three</li>
                    <li>
                      Nested ol
                      <ol>
                        <li>Two</li>
                        <li>Three</li>
                      </ol>
                    </li>
                  </ol>
                </li>
              </ol>
              <ul>
                <li>Four</li>
                <li>
                  <a href="#!">Five</a>
                </li>
                <li>Six</li>
                <li>
                  Nested ul
                  <ul>
                    <li>Two</li>
                    <li>Three</li>
                    <li>
                      Nested ol
                      <ol>
                        <li>Two</li>
                        <li>Three</li>
                        <li>
                          Nested ol
                          <ol>
                            <li>Two</li>
                            <li>Three</li>
                            <li>
                              Unordered
                              <ul>
                                <li>Thing</li>
                                <li>
                                  Nested ol
                                  <ol>
                                    <li>Two</li>
                                    <li>Three</li>
                                    <li>
                                      Nested ol
                                      <ol>
                                        <li>Two</li>
                                        <li>Three</li>
                                      </ol>
                                    </li>
                                  </ol>
                                </li>
                              </ul>
                            </li>
                          </ol>
                        </li>
                      </ol>
                    </li>
                  </ul>
                </li>
                <li>
                  Nested ol
                  <ol>
                    <li>Two</li>
                    <li>Three</li>
                    <li>
                      Nested ol
                      <ol>
                        <li>Two</li>
                        <li>Three</li>
                      </ol>
                    </li>
                  </ol>
                </li>
              </ul>
              <ol>
                <li>Two</li>
                <li>Three</li>
                <li>
                  Nested ol
                  <ol>
                    <li>Two</li>
                    <li>Three</li>
                  </ol>
                </li>
              </ol>
            </div>
            <section>
              <h1>{this.props.content.title}</h1>
            </section>
            <section id="profile">
              <h2>Profile</h2>
              <h2>
                We specialise in the intersection of design and development —
                where strategy and purpose meets execution and technology.
              </h2>
              <button>This is a button</button>
              <p>
                About <a href="#!">half of our work</a> is us in a design and
                development role, with the other half as development partner to
                design studios. In the latter we take more of a back seat role
                with respect to design but typically one where our digital
                design experience is appreciated. We get a kick out of both and
                the projects with the best outcomes are those that we're
                involved in early on in the process. In general we work closely
                as possible with the end client and this helps us facilitate the
                decision making process and react efficiently to changes in
                priority and the implications that leads to.
              </p>
            </section>
            <section id="contact">
              <h2>Contact</h2>
              {this.props.content.metadata.seo_image && (
                <Imgset {...this.props.content.metadata.seo_image} />
              )}
            </section>
            <section id="case-studies">
              <h2>Case Studies</h2>
            </section>
            <section id="selected-projects">
              <h2>Selected Projects</h2>
            </section>
            <section id="partners">
              <h2>Partners</h2>
            </section>
          </div>
        </div>
      </BaseLayout>
    )
  }
}

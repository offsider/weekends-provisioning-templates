import Imgset from '../../platform/UI/Imgset/Imgset.client'


export default function (container: Element) {
  Array.from(container.querySelectorAll('.Imgset')).map((node) => {
    Imgset(node as HTMLDivElement)
  })
}

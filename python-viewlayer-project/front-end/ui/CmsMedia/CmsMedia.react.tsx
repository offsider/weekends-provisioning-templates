import React, { Component } from "react"
import { images } from "../../cms-media/index"
import { ImgData } from "../../cms-data/ImgData"
import Imgset from "../../platform/UI/Imgset/Imgset.react"

export default function(props: any) {
  return (
    <div className="CmsMedia">
      {Object.keys(images).map(key => {
        const img = (images as { [key: string]: ImgData })[key]
        return (
          <figure>
            <Imgset {...img} sizes={{ 0: 320 }} />
            <figcaption>
              {key}
              <br />
              <small>{img.src}</small>
            </figcaption>
          </figure>
        )
      })}
    </div>
  )
}

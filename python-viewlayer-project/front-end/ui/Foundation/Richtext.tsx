import * as React from "react"
import * as DOM from "react-dom-factories"
const flexRenderer: any = require("../../platform/Utils/flexRenderer").default
import md, { MarkdownRenderOptions } from "../../platform/Utils/renderMarkdown"
const reactNodes: any = require("../../platform/Utils/reactNodes").default
import Imgset from "../../platform/UI/Imgset"

// import renderCO2eNicely from "./renderCO2eNicely"

export function inlineRenderMd(input: string) {
  return md(
    {
      profile: "inline",
      // post_processing: [renderCO2eNicely],
    },
    "react",
    input
  )
}

export function flexContentRender<T>(
  blocks: T[],
  mdOpts?: MarkdownRenderOptions,
  extraRenderers?: { [key: string]: (props: T) => any }
) {
  const finalOpts = Object.assign(
    {
      // post_processing: [renderCO2eNicely],
      heading_root: 2,
      allowed_tags: [
        "h2",
        "h3",
        "p",
        "strong",
        "em",
        "a",
        "ul",
        "ol",
        "li",
        "del",
      ],
    },
    mdOpts || {}
  )

  const rendered = flexRenderer(blocks, finalOpts)

  return rendered.map((block: any) => {
    switch (block.type) {
      case "MarkdownBlock":
        return reactNodes(block.html)
      case "InlineImagesRow":
        return (
          <figure className="InlineImagesRow">
            {block.data.map((block: any, ix: number) => {
              return (
                <img
                  key={ix}
                  className="InlineImagesRow__img"
                  alt={block.alt_text}
                  src={block.image.url}
                />
              )
            })}
          </figure>
        )

      case "SingleImage":
        return (
          <figure className="SingleImage">
            <div className="SingleImage__img">
              <Imgset {...block.data.image} />
            </div>
            {block.data.caption && (
              <figcaption className="SingleImage__caption">
                {block.data.caption}
              </figcaption>
            )}
          </figure>
        )
      default:
        if (extraRenderers && extraRenderers[block.type]) {
          return extraRenderers[block.type](block)
        } else {
          return <p>{`block renderer not found: ${block.type}`}</p>
        }
    }
  })
}

export function blockRenderMd(
  mdtext: string,
  heading_root?: number,
  allowed?: string[]
) {
  return md(
    {
      profile: "block",
      allowed_tags: allowed || null,
      heading_root: heading_root || 2,
      // post_processing: [renderCO2eNicely],
    },
    "react",
    mdtext
  )
}

export function blockAbstractRenderMd(abstractText: string) {
  return blockRenderMd(abstractText, 1, ["p", "strong", "em", "a"])
}

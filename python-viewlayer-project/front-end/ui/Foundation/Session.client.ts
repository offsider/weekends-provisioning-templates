let cart: any = []
let csrf_token: string = ""

export default function initSession() {
  const request = new XMLHttpRequest()
  request.onload = function(r) {
    if (request.status === 200) {
      const json = JSON.parse(request.response)
      csrf_token = json.csrf_token
      populateCSRFToken()
    }
  }
  request.open("GET", "/user/session/")
  request.send()
}

export function populateCSRFToken() {
  Array.from(document.querySelectorAll('[name="csrfmiddlewaretoken"]')).forEach(
    (input) => {
      console.log("populating csrf token", input)
      input.setAttribute("value", csrf_token)
    }
  )
}

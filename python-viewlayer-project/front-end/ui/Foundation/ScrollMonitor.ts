import debounce from '../../platform/Utils/debounce'
import throttle from '../../platform/Utils/throttle'
import {bind} from '../../platform/Utils'

class ScrollMonitor {
  private listeners: ((scrollY: number, scrollY2: number) => void)[]
  private clientHeight: number

  constructor() {
    this.listeners = []
    this.clientHeight = document.documentElement.clientHeight
  }

  start () {
    window.addEventListener('scroll', this.onScroll)
    window.addEventListener('resize', this.onResize)
  }

  @bind
  onResize () {
    this.clientHeight = document.documentElement.clientHeight
  }

  @bind
  onScroll () {
    const y = window.pageYOffset
    const y2 = y + this.clientHeight
    this.listeners.forEach((f) => f(y, y2))
  }

  addListener(func: (scrollY: number, scrollY2?: number) => void) {
    this.listeners.push(func)
  }

  removeListener(func: (scrollY: number, scrollY2?: number) => void) {
    this.listeners = this.listeners.filter((f) => f != func)
  }
}

const mon = new ScrollMonitor()

export default mon

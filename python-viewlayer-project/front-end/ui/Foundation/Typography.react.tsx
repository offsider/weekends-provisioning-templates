import React, { Component } from "react"

export default function(props: any) {
  return (
    <div className="Typography">
      <h1 className="title">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
      <h2 className="heading">Sed faucibus turpis vel tellus finibus aliquet</h2>
      <p className="body">
        Cras ut viverra dolor, faucibus vulputate justo. Integer vitae sapien
        eget libero vulputate gravida sed euismod nibh. Quisque gravida orci a
        orci viverra ultricies.
      </p>
      <p className="small">
        Sed faucibus turpis vel tellus finibus aliquet. Suspendisse consectetur
        tortor at augue posuere, eleifend suscipit tellus laoreet. Maecenas
        purus felis, placerat sit amet tempus ac, dictum at ex. Proin tempor
        faucibus felis. Integer lorem erat, sollicitudin nec convallis ac,
        ultrices vitae dui. Cras nec mauris semper risus luctus blandit. Etiam
        iaculis euismod efficitur.
      </p>
      <p className="body">
        In hendrerit laoreet ipsum ut vestibulum. Vestibulum tincidunt nibh
        tristique velit porta fermentum. Nunc a fringilla odio. Class aptent
        taciti sociosqu ad litora torquent per conubia nostra, per inceptos
        himenaeos. Aliquam cursus venenatis massa. Fusce eu sapien at augue
        faucibus vestibulum. Nulla convallis gravida nisi. Cras elementum justo
        dolor, non convallis nibh mollis suscipit. Donec in sapien rutrum,
        auctor lorem nec, iaculis lectus. Nunc condimentum orci nibh, sed
        posuere lectus suscipit id.
      </p>
      <p className="body">
        Pellentesque interdum laoreet odio sit amet pretium. Nam metus sem,
        lobortis vel mauris ac, porttitor ullamcorper orci. Donec egestas rutrum
        est sit amet fringilla. Phasellus non accumsan eros. Morbi ut pulvinar
        odio. Sed condimentum laoreet vestibulum. Aenean vestibulum, nulla vitae
        consequat posuere, nisl arcu efficitur risus, et tincidunt diam sem
        bibendum mi. Sed tincidunt arcu urna, in laoreet lorem congue quis.
        Proin volutpat pretium est eget semper. Aliquam sapien enim, malesuada
        molestie euismod quis, finibus a odio. Donec laoreet lorem nec libero
        aliquam pharetra.
      </p>
    </div>
  )
}

import find from 'lodash/find'

export type ViewParams = {[key: string]: string}
export interface RoutePath {
  name: string
  matcher: string
}

export interface RouteStack {
  rootPath: string
  routes: RoutePath[]
}

export function addRoute(
  routeStack: RouteStack,
  name: string,
  matcher: string,
): RouteStack {
  routeStack.routes.push({name, matcher})
  return routeStack
}


export function getUrl(
  routeStack: RouteStack,
  name: string,
  params?: ViewParams
) {
  const {routes} = routeStack
  const matchingRoute = find(routes, (route: RoutePath) => {
    return route.name == name
  })

  if (matchingRoute) {
    const segments = matchingRoute.matcher.split("/")
    const parsedSegments = segments.map((segment) => {
      if (segment[0] && segment[0] == "*") {
        if (!params) {
          return ""
        }
        const segmentParam = params[segment.slice(1)]
        if (segmentParam) {
          return segmentParam
        } else {
          return ""
        }
      } else if (segment[0] && segment[0] == ":") {
        const segmentParam = params[segment.slice(1)]
        if (segmentParam) {
          return segmentParam
        }
        console.warn(`Route named ${name} is missing a parameter ${segment.slice(1)}`)

        return 'parameter-not-found'
      } else {
        return segment
      }
    })

    return routeStack.rootPath + parsedSegments.join('/')
  } else {
    console.warn(`Route named ${name} not found`)
    return routeStack.rootPath + "/404-route-not-found"
  }
}

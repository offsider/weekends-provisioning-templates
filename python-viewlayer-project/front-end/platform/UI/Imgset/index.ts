import Imgset from './Imgset.react'
import {default as client} from './Imgset.client'

export default Imgset
export {client}

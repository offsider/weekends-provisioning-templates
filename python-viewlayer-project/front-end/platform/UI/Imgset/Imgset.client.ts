import debounce from '../../Utils/debounce'
import imageLoaded from '../../Utils/imageLoaded'


const ImgsetPrototype = {
  init: function (args: any) {
    // Arguments can be set in the markup on a data-json attribute
    // OR passed in as the second paremeter in the Create function
    const data = JSON.parse(this.el.children[0].getAttribute('data-json'))
    args = Object.assign(data, args || {})

    this.autoload = args.autoload !== undefined
      ? args.autoload
      : true
    this.ratio = args.ratio
    this.set = args.set
    this.focus = args.focus
    // this.alt = args.alt
    this.canSaveImg = args.canSaveImg !== undefined
      ? args.canSaveImg
      : false

    this.currentSrc = null
    this.shouldLoadImages = !!this.autoload
  },

  createMarkup: function () {
    this.image = document.createElement('span')
    this.image.className = 'Imgset__background'
    if (this.focus) {
      this.image.style.backgroundPosition = `${this.focus[0]*100}% ${this.focus[1]*100}%`
    }

    if (this.canSaveImg) {
      this.img = document.createElement('img')
      this.img.className = 'Imgset__img'
      this.image.appendChild(this.img)
    }

    this.el.appendChild(this.image)
  },

  onResize: function () {
    if (this.el.offsetWidth === 0) return
    if (!this.shouldLoadImages) return

    // Determine ideal image dimensions base on the ratio of the parent
    const forcedRatio = this.el.offsetHeight / this.el.offsetWidth;
    const imgWidth = this.ratio < forcedRatio
      ? this.el.offsetHeight / this.ratio
      : this.el.offsetWidth

    const uri = closestKey(this.set, imgWidth)

    // Avoid the overhead of imageLoaded unless we really need to
    if (this.image && uri === this.currentSrc) return
      this.currentSrc = uri

    imageLoaded(uri, () => {
      if (!this.shouldLoadImages) return
      if (!this.image) this.createMarkup()

      setTimeout(() => {
        this.image.style.backgroundImage = 'url(' + uri + ')'
        if (this.canSaveImg) this.img.src = uri
        this.el.className = this.el.className.trim() + ' is-loaded'
      }, 26)
    })
  },

  load: function (msg: any) {
    this.shouldLoadImages = true
    this.onResize()
  },

  unload: function () {
    this.shouldLoadImages = false
    this.el.className = this.el.className.split('is-loaded').join('').trim()
    if (this.image) {
      this.image.remove() // this.image.parentNode.removeChild(this.image) for better browser support
      this.image = false // removing the node doesn't mean this truthys to false
    }
  }
}


const ImgsetList: any[] = []


export default function (el: HTMLDivElement | HTMLSpanElement, args?: any) {
  const is = Object.create(ImgsetPrototype)
  is.el = el
  is.init(args || {})

  if (is.autoload) is.onResize()

  ImgsetList.push(is)

  // Bind a single resize event handler
  if (ImgsetList.length === 1) {
    window.addEventListener('resize', debounce(() => {
      ImgsetList.forEach(x => {
        x.onResize()
        // console.log(':)')
      })
    }, 150))
  }

  return is
}



// Finds the closest matched (but not lower) object key and returns the matching value
// Assumes object keys and target are ints
function closestKey (obj: any, target: any) {
  let closest = 0
  Object.keys(obj).forEach((key: any) => {
    if (key <= target) {
      closest = Math.max(closest, key)
    }
  })
  return obj[closest]
}

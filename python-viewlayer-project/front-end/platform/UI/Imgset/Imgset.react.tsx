import * as React from "react"
import * as DOM from "react-dom-factories"
import { ContextProps, withAppContext } from "../../AppContext"
import bem from "../../../platform/Utils/bem"

@withAppContext
export default class Imgset extends React.PureComponent<any, any> {
  static displayName = "Imgset"

  render() {
    const {
      autoload = true, // Load on DOM ready
      img_at_front = true,
      fill_parent = false,
      fillParent = false,
      fillMode = "cover",
      resizer,
      className,
      external = null, // if external, just accept it as a set
      background_color = null, // Supports average image color
      focus = [0.5, 0.5],
      src, // original src
      width, // original src width
      height, // original src height
      alt = "",
      quality = 90,
      sizes = {
        0: 1100,
        450: 1260,
        768: 1324,
        960: 1580,
        1024: 1900,
        1200: 2300,
        1600: 2700,
      },
      // ratio // See ratioString
    } = this.props

    // Ratio can be a height/width value like .5625
    // or a string like 19:9 where width:height
    const ratio = this.props.ratio
    const cx = bem("Imgset")

    const classNames: { [key: string]: boolean } = {
      "&": true,
      "&--fillParent": fill_parent || fillParent,
      "&--contain": fillMode == "contain",
    }

    if (className) {
      classNames[className] = true
    }

    const ratio_percentage = ratio
      ? ratio * 100 + "%"
      : (height / width) * 100 + "%"

    let styles: { [key: string]: any } = {
      paddingBottom: ratio_percentage,
    }

    if (background_color) {
      styles.backgroundColor = background_color
    }

    if (external) {
      return (
        <span style={styles} className={cx(classNames)}>
          <noscript
            data-json={JSON.stringify({
              ratio: ratio || height / width,
              set: external.set,
              alt: alt,
              focus: focus,
              autoload: autoload,
              img_at_front: img_at_front,
            })}
          >
            <img src={external.fallback_src} alt={alt} />
          </noscript>
        </span>
      )
    }

    // Generate images
    let set: { [key: string]: number } = {}
    if (src) {
      for (let prop in sizes) {
        set[prop] = resizer.resize(src, {
          width: sizes[prop],
          height: ratio ? sizes[prop] * ratio : sizes[prop] / (width / height),
          crop: "focus",
          focus: focus,
          quality: quality,
        })
      }
    }

    const fallback_src = resizer.resize(src, {
      width: 1024,
      height: ratio ? 1024 * ratio : 1024 / (width / height),
      crop: "focus",
      focus: focus,
    })

    return (
      <span style={styles} className={cx(classNames)}>
        <noscript
          data-json={JSON.stringify({
            ratio: ratio || height / width,
            set: set,
            alt: alt,
            focus: focus,
            autoload: autoload,
            img_at_front: img_at_front,
          })}
        >
          <img src={fallback_src} alt={alt} />
        </noscript>
      </span>
    )
  }
}

//- Size specific cropping. Useful if you want portrait cropped images on narrow screens
// function sizeWithXSeparator (size) {
//   if (
//     typeof size === 'string'
//     && (size.split('x')).length === 2
//   ) {
//     return size.split('x')[0]
//   } else {
//     return [
//       size,
//       size / (width / height)
//     ]
//   }
// }

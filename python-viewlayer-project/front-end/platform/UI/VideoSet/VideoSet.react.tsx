import * as React from "react"
import * as DOM from "react-dom-factories"
import { bind } from "../../../platform/Utils"
import { ContextProps, withAppContext } from "../../../platform/AppContext"
import bem from "../../../platform/Utils/bem"

export interface VideoSizeDefinition {
  webm: string
  mp4: string
}
export type VideoSetFiles = { [key: string]: VideoSizeDefinition }

export interface VideoSetProps {
  sizes?: { [key: string]: string }
  files: VideoSetFiles
  muted?: boolean
  playsInline?: boolean
  preload?: string
  autoPlay?: boolean
  loop?: boolean
  className?: string
  setSize?: string
}

export default class VideoSet extends React.PureComponent<VideoSetProps, {}> {
  static displayName = "VideoSet"

  render() {
    const cx = bem("VideoSet")

    const sizes = this.props.sizes || {
      "0": "sd",
      "800": "hd",
    }

    const classes: { [key: string]: boolean } = {
      "&": true,
    }

    if (this.props.className) {
      classes[this.props.className] = true
    }

    const setSize = this.props.setSize || "sd"

    return (
      <video
        muted={this.props.muted}
        playsInline={this.props.playsInline}
        preload={this.props.preload}
        loop={this.props.loop}
        autoPlay={this.props.autoPlay}
        className={cx(classes)}
        data-json={JSON.stringify({
          sizes: sizes,
          playsInline: this.props.playsInline,
          muted: this.props.muted,
          preload: this.props.preload,
          loop: this.props.loop,
          autoPlay: this.props.autoPlay,
          files: this.props.files,
        })}
      >
        <source src={this.props.files[setSize].mp4} type="video/mp4" />
        <source src={this.props.files[setSize].webm} type="video/webm" />
      </video>
    )
  }
}

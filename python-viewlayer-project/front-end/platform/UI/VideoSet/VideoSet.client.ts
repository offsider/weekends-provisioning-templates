export default function (scope: Element) {
  return Array.from(scope.querySelectorAll('.VideoSet')).map((node: HTMLVideoElement) => {
    const data = JSON.parse(node.getAttribute('data-json'))

    const sizeToUse = closestKey(data.sizes, document.documentElement.clientWidth)

    const filesToUse = data.files[sizeToUse]

    // const videoElem = document.createElement('video')
    // if (data.preload) {
    //   videoElem.setAttribute('preload', data.preload)
    // }
    // if (data.playsInline) {
    //   videoElem.setAttribute('playsinline', '')
    // }
    // if (data.loop) {
    //   videoElem.setAttribute('loop', '')
    // }
    // if (data.muted) {
    //   videoElem.setAttribute('muted', '')
    // }
    // node.appendChild(videoElem)

    // const mp4 = document.createElement('source')
    // mp4.setAttribute('src', filesToUse.mp4)
    // mp4.setAttribute('type', 'video/mp4')
    // videoElem.appendChild(mp4)

    // const webm = document.createElement('source')
    // webm.setAttribute('src', filesToUse.webm)
    // webm.setAttribute('type', 'video/webm')
    // videoElem.appendChild(webm)
  })
}



// Finds the closest matched (but not lower) object key and returns the matching value
// Assumes object keys and target are ints
function closestKey (obj: any, target: any) {
  let closest = 0
  Object.keys(obj).forEach((key: any) => {
    if (key <= target) {
      closest = Math.max(closest, key)
    }
  })
  return obj[closest]
}

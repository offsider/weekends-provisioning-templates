import { VideoSetProps } from "./VideoSet.react"

export const standard: VideoSetProps = {
  files: {
    sd: {
      mp4: "/assets/PackagedVideo/West-video-sd.mp4",
      webm: "/assets/PackagedVideo/West-video-sd.webm",
    },
    hd: {
      mp4: "/assets/PackagedVideo/West-video-hd.mp4",
      webm: "/assets/PackagedVideo/West-video-hd.webm",
    },
  },
}

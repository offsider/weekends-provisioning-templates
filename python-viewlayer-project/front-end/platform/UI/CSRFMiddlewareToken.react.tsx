import * as React from "react"
import * as DOM from "react-dom-factories"

export default class CSRFMiddlewareToken extends React.PureComponent<{}, {}> {
  static displayName = "CSRFMiddlewareToken"
  render() {
    return <input type="hidden" name="csrfmiddlewaretoken" value="" />
  }
}

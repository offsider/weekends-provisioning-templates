// import d from '../Loader/svg/d.sketch.min.svg';

module.exports["default"] = {
  svg: `<svg width="24" height="6" viewBox="0 0 24 6" xmlns="http://www.w3.org/2000/svg">
  <path
    d="M 0,0 c 3,0 3,6 6,6 c 3,0 3,-6 6,-6 c 3,0 3,6 6,6 c 3,0 3,-6 6,-6"
    stroke="#FF8A8A"
    stroke-width="1.5"
    fill="none"
    fill-rule="evenodd"/>
</svg>`,
  responsive: true,
}

import * as React from 'react'
import SVGO from 'svgo'


export default React.createClass({
  render: function() {
    let {
      svg,
      responsive = false,
      title,
      className,
      width,
      forceViewBox,
      height,
      optomise = true,
    } = this.props

    if (optomise) {
      // Optimise SVG with SVGO
      // * This is especially important when dealing with user uploaded SVGs that may contain style attributes that will choke React
      const svgo = new SVGO({
        convertStyleToAttrs: true, // *
        cleanupAttrs: true, // cleanup attributes from newlines, trailing and repeating spaces
        removeTitle: true,
        removeDescription: true,
      })

      // Set the optimized SVG and width/height dimensions read from the file
      svgo.optimize(svg, function(result) {
        svg = result.data
        width = width || result.info.width
        height = height || result.info.height
      })
    }

    // Rip <svg> tag off.
    // Regex only works with the cleanupAttrs svgo prop set since it expects no new lines
    const matches = svg
      ? svg.match(/<svg( [^<>]+)?>(.+?)<\/svg>/)
      : null

    if (! matches) {
      console.log('Malformed SVG')
      console.log(svg)
      return React.DOM.span({}, 'Malformed SVG')
    }

    const attr = matches[1] // svg attrs; width="blah" etc
    let paths = matches[2]  // svg paths/groups

    // Turn svg attributes into object
    const attrParts = attr.match(/(.+?)="(.+?)"/g)
    let attrObj = {}
    if (attrParts) {
      attrParts.forEach(function(part){
        if ((part = part.trim())) {
          part = part.match(/(.+?)="(.+?)"/)
          attrObj[(part[1] === 'class' ? 'className' : part[1])] = part[2]
        }
      })
    }

    // If no width is set use viewbox dimensions as fallback
    if (!width)
      width = attrObj.viewBox.split(' ')[2]
    if (!height)
      height = attrObj.viewBox.split(' ')[3]

    // Write the viewBox, width and height to the attrObj
    attrObj.width = width
    attrObj.height = height
    attrObj.viewBox = '0 0 ' + width + ' ' + height

    if (forceViewBox) {
      attrObj.viewBox = forceViewBox
    }

    if (title) {
      // Smash our new title in the front. I know, super elegant.
      paths = '<title>' + title + '</title>' + paths
    }

    if (responsive) {
      return React.DOM.span({
        className: 'Svg',
        style: {
          paddingBottom: (height / width * 100) + '%'
        }
      },
        React.DOM.svg(Object.assign(attrObj, {
          className: className + ' Svg__svg',
          dangerouslySetInnerHTML: { __html: paths },
        }))
      )
    } else {
      return React.DOM.svg(Object.assign(attrObj, {
        className: className,
        dangerouslySetInnerHTML: { __html: paths },
      }))
    }
  }
})

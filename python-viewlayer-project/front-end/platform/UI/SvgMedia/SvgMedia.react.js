import * as React from 'react'
import {ContextComponent, ROUTES} from '../../_'
import Svg from '../Svg'

export default class Nav extends ContextComponent {
  static displayName = 'SvgMedia'

  render () {
    const {src} = this.props
    const {loadSvgMedia} = this.getContext()

    const svg = loadSvgMedia(src)

    const nextProps = Object.assign({
      svg,
    }, this.props);

    return React.createElement(Svg, nextProps);
  }
}

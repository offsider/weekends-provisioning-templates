export function post (url: string, data?: any) {
  return new Promise((resolve, reject) => {
    function onComplete () {
      const contentType = req.getResponseHeader('content-type')
      if (contentType == 'application/json') {
        if (req.status >= 400) {
          reject(JSON.parse(req.responseText))
        } else {
          resolve(JSON.parse(req.responseText))
        }
      } else {
        if (req.status >= 400) {
          reject(req.responseText)
        } else {
          resolve(req.responseText)
        }
      }
    }

    function onError () {
      reject({"status": "fail"})
    }

    const req = new XMLHttpRequest()

    req.addEventListener("load", onComplete)
    req.addEventListener("error", onError)

    req.open("POST", url, true)

    if (data) {
      req.setRequestHeader('content-type', 'application/json')
      req.send(JSON.stringify(data))
    } else {
      req.send()
    }
  })
}

import * as React from "react"
import * as DOM from "react-dom-factories"
import SpawnLocalizeContext, { LocalizeContext } from "../Localize"
import embedDataSvg from "../Utils/embedDataSvg"

export const AppContext = React.createContext({
  t_: (key: string, params?: any) => "translation not loaded",
  l10n: {},
  pdf_render: false,
  resizer: null,
  getUrl: (key: string, params?: any) => "#routes-not-loaded",
})
const Provider: any = AppContext.Provider

export interface ContextProps {
  t_?: (key: string, params?: any) => string
  l10n?: LocalizeContext
  pdf_render?: boolean
  resizer?: any
  getUrl?: (key: string, params?: any) => string
}

export function embedWhenUsedInPDF<T extends React.ReactElement<any>>(
  func: (...args: any[]) => T
) {
  const Component: any = func
  const FunctionalComponent = function(...args: any[]) {
    return (
      <AppContext.Consumer>
        {(context) => {
          const elem = Component(...args)

          const width = elem.props.width
          const height = elem.props.height
          const className: string = elem.props.className

          if (context.pdf_render) {
            return embedDataSvg(elem, width, height, className)
          } else {
            return elem
          }
        }}
      </AppContext.Consumer>
    )
  }
  ;(FunctionalComponent as any).displayName = Component.displayName
  return FunctionalComponent
}

export function withAppContext<T extends React.ComponentType>(constructor: T) {
  const Component: any = constructor
  const FunctionalComponent = function(props: any) {
    return (
      <AppContext.Consumer>
        {(context) => {
          return (
            <Component
              {...props}
              t_={context.t_}
              l10n={context.l10n}
              resizer={context.resizer}
              getUrl={context.getUrl}
            />
          )
        }}
      </AppContext.Consumer>
    )
  }
  ;(FunctionalComponent as any).displayName = Component.displayName
  return FunctionalComponent as T
}

interface AppContextProps {
  lang: string
  resizer: any
  pdf_render: boolean
  getUrl: (key: string, params?: any) => string
}

export default class AppRoot extends React.Component<AppContextProps, {}> {
  private localizeContext: LocalizeContext
  private contextValue: ContextProps

  constructor(props: AppContextProps) {
    super(props)

    this.localizeContext = SpawnLocalizeContext(props.lang)
    this.updateContextValue(props)
  }

  updateContextValue(props: AppContextProps) {
    this.contextValue = {
      t_: this.localizeContext.translate,
      l10n: this.localizeContext,
      resizer: this.props.resizer,
      pdf_render: this.props.pdf_render,
      getUrl: this.props.getUrl,
    }
  }

  componentWillReceiveProps(nextProps: AppContextProps) {
    if (nextProps.lang !== this.props.lang) {
      this.localizeContext = SpawnLocalizeContext(nextProps.lang)
      this.updateContextValue(nextProps)
    }
  }

  render() {
    return <Provider value={this.contextValue}>{this.props.children}</Provider>
  }
}

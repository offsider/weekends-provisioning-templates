export default function animate(renderCb:(num: number) => void, duration:number, ease?:Function) {
  ease = ease || ((t:number) => t)
  let elapsedTime = 0
  let elapsedPercent = 0
  let stopped = false

  _animate()
  function _animate() {
    if (stopped) { return false }

    if (elapsedTime < duration) {
      elapsedPercent = ease(elapsedTime / duration) as number
      renderCb(elapsedPercent)
      elapsedTime += 1000 / 60
      requestAnimationFrame(_animate) // Start next frame
    } else {
      renderCb(1)
    }
  }

  return {
    stop() {
      stopped = true
    }
  }
}

export function predictAnimate(renderCb:(num: number) => void, duration:number, frameRate: number, ease?:Function) {
  ease = ease || ((t:number) => t)
  let elapsedTime = 0
  let elapsedPercent = 0
  let stopped = false

  while (elapsedTime < duration) {
    _animate()
  }

  return elapsedTime

  function _animate() {
    if (elapsedTime < duration) {
      elapsedPercent = ease(elapsedTime / duration) as number
      renderCb(elapsedPercent)
      elapsedTime += frameRate
    } else {
      renderCb(1)
    }
  }
}

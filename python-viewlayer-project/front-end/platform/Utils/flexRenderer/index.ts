import md, { MarkdownRenderOptions } from "../renderMarkdown"

interface BlockShape {
  type: string
  data: any
  html?: string
}

const flexRenderer = function(
  blocks: BlockShape[],
  mdOpts: MarkdownRenderOptions
) {
  const mdRenderer = md(mdOpts, "string")

  const BREAK_TOKEN = "<!-- break markdown -->"
  const MARKDOWN_BLOCK = "MarkdownBlock"

  let texts: string[] = []
  blocks.map((block) => {
    if (block.type === MARKDOWN_BLOCK && block.data) {
      texts.push(block.data)
      texts.push("\n" + BREAK_TOKEN)
    }
  })

  let html = (mdRenderer(texts.join("\n")) as string).split(BREAK_TOKEN)

  return blocks.map((block) => {
    if (block.type === MARKDOWN_BLOCK && block.data) {
      return {
        type: block.type,
        data: block.data,
        html: html.shift(),
      }
    } else {
      return block
    }
  })
}

export default flexRenderer

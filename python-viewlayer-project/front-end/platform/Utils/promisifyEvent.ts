export default function promisifyEvent(el: HTMLElement, event: string) {
  return new Promise((resolve) => {
    function end() {
      el.removeEventListener(event, end)
      resolve()
    }
    el.addEventListener(event, end)
  })
}

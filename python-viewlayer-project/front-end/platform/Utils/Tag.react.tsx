import * as React from "react"
import { ContextProps, withAppContext } from "../AppContext"

export interface TagProps extends React.AllHTMLAttributes<any> {
  el: string
}

export default class Tag extends React.PureComponent<TagProps, {}> {
  static displayName = "Tag"
  render() {
    const props: any = Object.assign({}, this.props)
    delete props.el
    return React.createElement(this.props.el, props)
  }
}

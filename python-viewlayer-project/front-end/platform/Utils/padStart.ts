export default function (
  str: string | number,
  length: number,
  pad: string | number = "0") {
  return Array(Math.max(length - String(str).length + 1, 0)).join(pad.toString()) + str;
}

// clamp
// return a variable, clamping between a maximum and minimum

export default function(min: number, value: number, max: number) {
  if (min != null && min != undefined && value < min) return min;
  if (max != null && max != undefined && value > max) return max;
  return value;
}

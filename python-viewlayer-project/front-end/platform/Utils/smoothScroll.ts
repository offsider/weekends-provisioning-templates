/*
Changelog
---------

12.08.2018
- smoothScrollElement created DW

18.07.2016
- Removes element from params in favour of y position. In 99% we're already using something like jQuery/Cash-dom and have easy access to elements and their offsets DW
*/

// AnimateScroll.js
// Sunmock Yang Nov. 2015

// https://github.com/sunmockyang/animate-scroll-js
// commit hash: a959281

import {linear} from './easing'

export default function (
  y: number | undefined,
  duration = 0,
  easing = linear,
  padding = 0,
  onFinish?: any
) {
  var docElem = document.documentElement; // to facilitate minification better
  var windowHeight = window.innerHeight;
  var maxScroll = docElem.scrollHeight - windowHeight;
  var currentY = window.pageYOffset;

  var targetY = y;
  targetY -= padding;
  targetY = Math.max(Math.min(maxScroll, targetY), 0);

  var deltaY = targetY - currentY;

  var obj = {
    targetY: targetY,
    deltaY: deltaY,
    duration: duration,
    easing: easing,
    onFinish: onFinish,
    startTime: Date.now(),
    lastY: currentY,
    step: step,
  };

  if (window.requestAnimationFrame) {
    window.requestAnimationFrame(obj.step.bind(obj));
  } else {
    onFinish();
  }
}

const step = function() {
  if (this.lastY != window.pageYOffset && this.onFinish) {
    this.onFinish();
    return;
  }

  // Calculate how much time has passed
  var t = Math.min((Date.now() - this.startTime) / this.duration, 1);

  // Scroll window amount determined by easing
  var y = this.targetY - (1 - this.easing(t)) * this.deltaY;
  window.scrollTo(window.pageXOffset, y);

  // Continue animation as long as duration hasn't surpassed
  if (t != 1) {
    this.lastY = window.pageYOffset;
    window.requestAnimationFrame(this.step.bind(this));
  } else {
    if (this.onFinish) this.onFinish();
  }
};


interface smoothScrollElementArgs {
  el: HTMLElement
  x?: number | undefined
  y?: number | undefined
  duration?: number
  easing?: Function
  paddingX?: number
  paddingY?: number
}

export function smoothScrollElement (args: smoothScrollElementArgs) {
  const {
    el,
    x = undefined,
    y = undefined,
    duration = 0,
    easing = linear,
    paddingX = 0,
    paddingY = 0,
  } = args

  const maxScrollX = el.scrollWidth - el.clientWidth
  const maxScrollY = el.scrollHeight - el.clientHeight
  const currentX = el.scrollLeft
  const currentY = el.scrollTop

  let targetX = x !== undefined ? x : currentX
  targetX -= paddingX
  targetX = Math.max(Math.min(maxScrollX, targetX), 0)
  let targetY = y !== undefined ? x : currentY
  targetY -= paddingY
  targetY = Math.max(Math.min(maxScrollY, targetY), 0)

  const deltaX = targetX - currentX;
  const deltaY = targetY - currentY;

  const startTime = Date.now()

  window.requestAnimationFrame(step)

  function step () {
    // Calculate how much time has passed
    let t = Math.min((Date.now() - startTime) / duration, 1);

    // Scroll window amount determined by easing
    const x = targetX - (1 - easing(t)) * deltaX;
    const y = targetY - (1 - easing(t)) * deltaY;
    el.scrollLeft = x
    el.scrollTop = y

    // Continue animation as long as duration hasn't surpassed
    if (t !== 1) {
      window.requestAnimationFrame(step)
    }
  }
}

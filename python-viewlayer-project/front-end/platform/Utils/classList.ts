/*
  IE's Element.classList:
  - doesn't support the `force` argument on `toggle`
  - doesn't support multiple arguments for `add` and `remove`
  - doesn't support `replace`

  Note that there's no IE support for Element.classList in IE9 and older.

  https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
*/
export default function classList(el: HTMLElement) {
  return {
    toggle(className: string, force: boolean) {
      if (force !== undefined) {
        force ? el.classList.add(className) : el.classList.remove(className)
      } else {
        classList(el).contains(className)
          ? el.classList.remove(className)
          : el.classList.add(className)
      }
    },
    add(...args: string[]) {
      args.forEach((className) => el.classList.add(className))
    },
    remove(...args: string[]) {
      args.forEach((className) => el.classList.remove(className))
    },
    item() {
      console.error("classList().item() not supported")
    },
    contains(className: string) {
      return el.classList.contains(className)
    },
    replace(oldClass: string, newClass: string) {
      el.classList.remove(oldClass)
      el.classList.add(newClass)
    },
  }
}

import * as React from "react"

export default function nl2br(text: string) {
  const lines = text.split("\n")
  return lines.reduce((out: any[], line: string) => {
    if (out.length > 0) {
      return out.concat([<br />, line])
    } else {
      return out.concat([line])
    }
  }, [])
}

export default function (str: string, returnBody: boolean): [string, HTMLCollection | HTMLElement] {
  // title required for use in IE10
  const temp = document.implementation.createHTMLDocument('tempDoc')
  temp.documentElement.innerHTML = str

  if (returnBody) {
    return [temp.title, temp.body]
  } else {
    return [temp.title, temp.body.children]
  }
}

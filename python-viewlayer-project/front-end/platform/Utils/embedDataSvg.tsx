import * as React from "react"
import * as DOM from "react-dom-factories"
import * as ReactDOMServer from "react-dom/server"
import { Buffer } from "buffer"

export default function(
  dataElem: React.ReactElement<any>,
  width: string | number,
  height: string | number,
  className: string
) {
  const svgString = ReactDOMServer.renderToString(dataElem)
  const dataUrl = `data:image/svg+xml;charset=utf-8;base64,${Buffer.from(
    svgString
  ).toString("base64")}`

  return (
    <embed type="image/svg+xml" src={dataUrl} width={width} height={height} className={className} />
  )
}

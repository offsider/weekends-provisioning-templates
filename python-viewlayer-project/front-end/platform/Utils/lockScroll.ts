/*
  This works on iOS and genuinely prevents scrolling on the html element when activated.

  However, this means that if browser chrome is hidden at the time the lock is activiate, to show the chrome users must either tap an invisible 5px strip at the bottom of the screen or tap the minimised strip of browser chrome. Both actions feel foreign to this long-time iOS user.

  Note too that tapping the time at the top left of the iOS device **does** scroll the user to the top of the document, ignoring the lock.
*/

import closest from "./closest"
import clamp from "./clamp"


let locked = false
let scrollElem: HTMLElement = null
let scrollElemScrollMax: number = undefined
let touchstartInScrollElem: HTMLElement = null


function lock(scrollableElem?: any) {
  locked = true

  // Prevent scroll on iOS devices
  scrollElem = scrollableElem
  window.addEventListener("touchstart", onTouchstart, { passive: false })
  window.addEventListener("touchmove", onTouchmove, { passive: false })

  // Prevent scroll on desktop devices
  document.documentElement.style.overflow = "hidden"
}

function unlock() {
  if (locked) {
    locked = false
    window.removeEventListener("touchstart", onTouchstart)
    window.removeEventListener("touchmove", onTouchmove)
    document.documentElement.style.overflow = ""
  }
}

function onTouchstart(e: TouchEvent) {
  touchstartInScrollElem = closest(
    e.target as HTMLElement,
    (el) => el === scrollElem
  )
  // In iOS, even if an element is scrollable (has overflow) if you scroll in a direction where no scroll is possible (when you're already scrolled to the very top or the very bottom) that scroll is passed through the the html elment.
  // Rather than detecting scroll direction, on the initial touchstart we bump the scroll position 11px down or up as needed to ensure you can always scroll at least 1px in either vertical direction
  scrollElemScrollMax = scrollElem.scrollHeight - scrollElem.offsetHeight
  scrollElem.scrollTop = clamp(1, scrollElem.scrollTop, scrollElemScrollMax - 1)

  // We can't distribute 1px to allow scrolling 0.5px in both vertical directions, so it's better to act as though no scroll is possible
  if (scrollElemScrollMax === 1) {
    scrollElemScrollMax = 0
    scrollElem.scrollTop = 0
  }
}

function onTouchmove(e: TouchEvent) {
  if (locked) {
    if (!touchstartInScrollElem) {
      e.preventDefault()
    }
    // In iOS an element that can't scroll (has no overflow) will pass the scroll through to the html element to scroll
    if (touchstartInScrollElem && scrollElemScrollMax === 0) {
      e.preventDefault()
    }
  }
}

export default {
  lock: lock,
  unlock: unlock,
}

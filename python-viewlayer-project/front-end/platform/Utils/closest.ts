export default function (el: HTMLElement, match: (el: HTMLElement) => boolean): HTMLElement | null {
  const html = document.documentElement
  while (!match(el) && el.parentNode && el !== html) {
    el = (el.parentNode as HTMLElement)
  }
  return match(el) ? el : null
}

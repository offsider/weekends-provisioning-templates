const reactNodes: any = require("../reactNodes").default
import marked from "marked"

const lexer = new marked.Lexer()
const renderer = new marked.Renderer()

const marked_options = {
  sanitize: true,
  // sanitize: false, // we need to allow <!-- markdown break tokens --> without sanitising them. Instead we'll manually remove any html that's not a html comment
  gfm: true, // enables ~~ for <del>. This also allows tables within marked.js but currently we're not supporting any markdown for tables via this API.
  tables: true,
  smartLists: true,
  smartypants: true, // "smart" typograhic punctuation for things like quotes and dashes.
  renderer: renderer,
}

const parser = new marked.Parser(marked_options)

const profiles: { [key: string]: { [key: string]: any } } = {
  base: {
    code: renderer.code,
    blockquote: renderer.blockquote,
    html: renderer.html,
    heading: renderer.heading,
    hr: renderer.hr,
    list: renderer.list,
    listitem: renderer.listitem,
    paragraph: renderer.paragraph,
    table: renderer.table,
    tablerow: renderer.tablerow,
    tablecell: renderer.tablecell,

    strong: renderer.strong,
    em: renderer.em,
    codespan: renderer.codespan,
    br: renderer.br,
    del: renderer.del,
    link: renderer.link,
    image: renderer.image,
    text: renderer.text,
  },
  inline: {
    code: none,
    blockquote: noop,
    html: none,
    heading: noop,
    hr: noop,
    list: noop,
    listitem: noop,
    paragraph: noop,
    table: none,
    tablerow: none,
    tablecell: none,
  },
  block: {},
}

type OutputChoices = "react" | "string"

export interface MarkdownRenderOptions {
  profile?: string // or base
  allowed_tags?: string[]
  heading_root?: number
  render_overrides?: { [key: string]: (...args: any[]) => string }
  post_processing?: Array<(html: string) => string>
}

export function render(
  opts: MarkdownRenderOptions,
  output: OutputChoices,
  markdown: string
) {
  const activeProfile = profiles[opts.profile || "base"]
  const headingRoot = opts.heading_root || 1
  const allowed_tags = opts.allowed_tags || []
  const resetProfile = profiles["base"]
  Object.keys(resetProfile).forEach((key) => {
    ;(renderer as any)[key] = resetProfile[key]
  })

  if (activeProfile) {
    Object.keys(activeProfile).forEach((key) => {
      ;(renderer as any)[key] = activeProfile[key]
    })
  }

  let tokens = lexer.lex(markdown)

  tokens.forEach((token: any) => {
    switch (token.type) {
      case "heading":
        token.depth = token.depth + headingRoot - 1

        if (!allowed(`h${token.depth}`)) {
          let depth = token.depth
          while (!allowed(`h${depth}`) && depth != 0) {
            depth--
          }

          if (depth == 0) {
            token.type = "paragraph"
          } else {
            token.depth = depth
          }
        }
    }
  })

  if (allowed_tags.length > 0) {
    // only restrict down to allowed tags if they're specified, otherwise
    // accept all tags as rendered by the profile

    if (!allowed("em")) {
      renderer.em = allowed("strong") ? resetProfile.strong : noop
    }

    if (!allowed("strong")) {
      renderer.strong = allowed("em") ? resetProfile.em : noop
    }

    if (!allowed("del")) {
      renderer.del = noop
    }

    if (!allowed("a")) {
      renderer.link = (url: string, title: string, text: string) => {
        return `[${text}](${url})`
      }
    }
  }

  if (opts.render_overrides) {
    Object.keys(opts.render_overrides).forEach((key) => {
      ;(renderer as any)[key] = opts.render_overrides[key]
    })
  }

  let html = parser.parse(tokens)

  if (opts.post_processing) {
    opts.post_processing.forEach((func) => {
      html = func(html)
    })
  }

  return output == "react" ? reactNodes(html) : html

  function allowed(tag: string) {
    return allowed_tags.length == 0 || allowed_tags.indexOf(tag) != -1
  }
}

type RenderOutput = string | React.ReactElement<any>

function curriedRender(
  opts: MarkdownRenderOptions
): (output: OutputChoices, md: string) => RenderOutput
function curriedRender(
  opts: MarkdownRenderOptions,
  out: OutputChoices
): (md: string) => RenderOutput
function curriedRender(
  opts: MarkdownRenderOptions,
  out: OutputChoices,
  md: string
): RenderOutput
function curriedRender(
  opts: MarkdownRenderOptions,
  out?: OutputChoices,
  md?: string
): any {
  if (opts && out && md) {
    return render(opts, out, md)
  } else if (opts && out) {
    return (callMd: string) => curriedRender(opts, out, callMd)
  } else if (opts) {
    return (callOutput: OutputChoices, callMd: string) =>
      curriedRender(opts, callOutput, callMd)
  } else {
    return render
  }
}

export default curriedRender

function noop(text: string) {
  return text
}

function none() {
  return ""
}

import * as React from "react"
import DOM from "react-dom-factories"

let jsdom, inBrowser
if (typeof window != "undefined") {
  inBrowser = true
} else {
  jsdom = require("jsdom")
}

const NODE_TYPES = {
  1: "ELEMENT_NODE",
  2: "ATTRIBUTE_NODE",
  3: "TEXT_NODE",
  4: "CDATA_SECTION_NODE",
  5: "ENTITY_REFERENCE_NODE",
  6: "ENTITY_NODE",
  7: "PROCESSING_INSTRUCTION_NODE",
  8: "COMMENT_NODE",
  9: "DOCUMENT_NODE",
  10: "DOCUMENT_TYPE_NODE",
  11: "DOCUMENT_FRAGMENT_NODE",
  12: "NOTATION_NODE",
}

const dontSetHtmlTags = ["hr"]

export default function(html) {
  let rootElem
  if (!inBrowser) {
    const { JSDOM } = jsdom
    const dom = new JSDOM(`<!DOCTYPE html><body>${html}</body>`)
    rootElem = dom.window.document.body
  } else {
    rootElem = window.document.createElement("div")
    rootElem.style.display = "none"
    rootElem.innerHTML = html
    window.document.body.appendChild(rootElem)
  }

  const nodes = Array.prototype.map.call(rootElem.childNodes, (node, ix) => {
    switch (NODE_TYPES[node.nodeType]) {
      case "TEXT_NODE":
        return node.textContent

      case "ELEMENT_NODE":
        let tagName = node.tagName.toString().toLowerCase()
        const classList = node.getAttribute("class")
        return DOM[tagName]({
          key: ix,
          className: classList || null,
          dangerouslySetInnerHTML:
            dontSetHtmlTags.indexOf(tagName) == -1
              ? { __html: node.innerHTML }
              : null,
        })

      default:
        return ""
    }
  })

  if (inBrowser) {
    rootElem.parentNode.removeChild(rootElem)
  }

  return nodes
}

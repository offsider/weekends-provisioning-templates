import smoothScroll from "./smoothScroll"
import { easeInOutCubic } from "./easing"

export default function onPageLinks(links: HTMLAnchorElement[]) {
  links.forEach((link) => {
    link.addEventListener("click", onClick)
  })

  return () => {
    links.forEach((link) => {
      link.removeEventListener("click", onClick)
    })
  }

  function onClick(e: Event) {
    const target = document.querySelector(
      (e.currentTarget as HTMLAnchorElement).getAttribute("href")
    )
    if (target) {
      e.preventDefault()
      const yOffset = target.getBoundingClientRect().top + window.scrollY
      smoothScroll(yOffset, 500, easeInOutCubic)
    }
  }
}

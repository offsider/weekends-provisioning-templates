export default function (scroller: HTMLElement) {
  const drag = {
    dragClickThreshold: 5, // px
    isDragging: false,
    _dragClickThresholdMet: false,
    startPageX: undefined as number | undefined,
    startScrollX: undefined as number | undefined,

    lastTime: undefined as number | undefined,
    lastX: undefined as number | undefined,
    xPerFrame: 0 as number,
    timePerFrame: 0 as number,
    velocityAnimation: undefined as any,

    start(e: MouseEvent | TouchEvent) {
      drag.isDragging = true
      drag.onStartCallback(e)
      drag.onMove(e)
      if (drag.velocityAnimation) {
        drag.velocityAnimation.stop()
      }
      document.body.addEventListener('mousemove', drag.onMove)
      document.body.addEventListener('touchmove', drag.onMove)
      document.body.addEventListener('mouseup', drag.end)
      document.body.addEventListener('mouseleave', drag.end)
      document.body.addEventListener('touchend', drag.end)
    },
    onMove(e: MouseEvent | TouchEvent) {
      const posX = getEventPositionX(e)

      if (drag.isDragging) {
        if (drag.startPageX === undefined) {
          drag.lastX = posX
          drag.startPageX = posX
          drag.startScrollX = scroller.scrollLeft
          drag._dragClickThresholdMet = false
          scroller.classList.add('is-dragging')
        }
        drag.onMoveCallback(e, posX)

        requestAnimationFrame((timestamp) => {
          drag.xPerFrame = posX - drag.lastX
          if (drag.lastTime && drag.lastTime != timestamp) {
            drag.timePerFrame = timestamp - drag.lastTime
          }
          drag.lastTime = timestamp
          drag.lastX = posX
        })
      }
    },
    end(e: MouseEvent) {
      if (drag.isDragging) {
        drag.preventDefaultIfDragged(e)
        drag.onEndCallback(e)
        drag.isDragging = false
        drag.startPageX = undefined
        drag.startScrollX = undefined
        scroller.classList.remove('is-dragging')
      }

      document.body.removeEventListener('mousemove', drag.onMove)
      document.body.removeEventListener('touchmove', drag.onMove)
      document.body.removeEventListener('mouseup', drag.end)
      document.body.removeEventListener('mouseleave', drag.end)
      document.body.removeEventListener('touchend', drag.end)
    },
    preventDefaultIfDragged(e: MouseEvent | TouchEvent) {
      const posX = getEventPositionX(e)
      if (
        posX > drag.startPageX + drag.dragClickThreshold ||
        posX < drag.startPageX - drag.dragClickThreshold
      ) {
        drag._dragClickThresholdMet = true
      }
    },
    onStartCallback(e: MouseEvent | TouchEvent) { },
    onMoveCallback(e: MouseEvent | TouchEvent, posX: number) { },
    onEndCallback(e: MouseEvent | TouchEvent) { },
  }

  // Prevent clicks on links
  ;(Array.from(scroller.querySelectorAll('a')) as HTMLAnchorElement[]).forEach(link => {
    link.addEventListener('mousedown', (e) => {
      e.preventDefault()
    })
    link.addEventListener('click', (e) => {
      if (drag._dragClickThresholdMet) {
        e.preventDefault()
      }
    })
  })


  return drag
}

export function getEventPositionX (e: MouseEvent | TouchEvent): number {
  if (e.type.indexOf('mouse') == 0) {
    return (e as MouseEvent).pageX
  } else {
    if ((e as TouchEvent).touches.length > 0) {
      return (e as TouchEvent).touches[0].pageX
    } else {
      return NaN
    }
  }
}

export function getEventPositionY (e: MouseEvent | TouchEvent): number {
  if (e.type.indexOf('mouse') == 0) {
    return (e as MouseEvent).pageY
  } else {
    if ((e as TouchEvent).touches.length > 0) {
      return (e as TouchEvent).touches[0].pageY
    } else {
      return NaN
    }
  }
}




export default function (src: string, callback: (result: string | Error) => void) {
  let image = new Image()
  image.onload = () => {
    callback(src)
    // @TODO destoy image perhaps? Any perf benefit?
  }

  image.onerror = () => {
    let msg = 'Could not load image src ' + src
    callback(new Error(msg))
  }

  image.src = src
}

import {createStore, combineReducers, Store} from 'redux'

export interface StateAction {
  type: string
}

export type ActionsBundle<T> = {
  [P in keyof T]: T[P]
}

type Reducer<T> = (state: T, action: StateAction) => T

export interface StateBundle<T> {
  name: string,
  initial: T,
  actions: {[key: string]: any}
  constants: {[key: string]: string}
  reducer: Reducer<T>
}

export default function createStateStore<AppState, AppActions> (stateBundles: StateBundle<any>[]) {
  const reducerStack: {[key: string]: Reducer<any>} = {}
  const initialState: {[key: string]: any} = {}
  const actionsStack: {[key: string]: {[key: string]: any}} = {}
  stateBundles.forEach((stateBundle) => { 
    reducerStack[stateBundle.name] = stateBundle.reducer
    initialState[stateBundle.name] = stateBundle.initial
  })

  const primaryReducer = combineReducers(reducerStack)
  const store: Store<AppState> = createStore(
    primaryReducer,
    initialState,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
  )

  const actionsBundle: any = {}
  stateBundles.forEach((stateBundle) => { 
    actionsBundle[stateBundle.name] = {}
    Object.keys(stateBundle.actions).forEach((actionKey: string) => {
      const func = stateBundle.actions[actionKey]
      actionsBundle[stateBundle.name][actionKey] = function (...args: any[]) {
        return store.dispatch(func(...args))
      }
    })
  })

  return {store, actionsBundle: (actionsBundle as AppActions)}
}

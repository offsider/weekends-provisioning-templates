import i18next from "i18next"
import { bind } from "./../Utils"
import { DateTime, Info } from "luxon"

export default function SpawnLocalizeContext(lang: string) {
  return new LocalizeContext(lang)
}

const enJson: any = require("./en.json")

export class LocalizeContext {
  private i18n: i18next.i18n

  constructor(private langCode: string) {
    this.i18n = i18next.init({
      lng: langCode,
      resources: {
        en: enJson,
      },
    })
  }

  @bind
  translate(key: string, params?: any) {
    return this.i18n.t(key, params)
  }

  // DATE AND TIME FORMATTING
  @bind
  formatDate(dateStr: string, format: string = "full") {
    const date = DateTime.fromISO(dateStr).setLocale(this.langCode)

    switch (format) {
      case "abbr":
        return date.toLocaleString(DateTime.DATE_SHORT)
      case "full":
        return date.toLocaleString(DateTime.DATE_FULL)
      case "year":
        return date.year.toLocaleString()
      // case "abbr":
      //   return date.toFormat('dd.MM.yyyy')
      // case "full":
      //   return date.toFormat('d. MMMM yyyy')
    }

    return date
  }

  // NUMBER PAD
  @bind
  numberPad(num: number, pad: number) {
    return Intl.NumberFormat("de", { minimumIntegerDigits: pad }).format(num)
  }

  formatNumLong(num: number) {
    return new Intl.NumberFormat("en-AU", {
      useGrouping: true,
      style: "decimal",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    })
      .format(num)
      .replace(/,/g, " ")
  }

  formatNumShort(num: number) {
    return new Intl.NumberFormat("en-AU", {
      useGrouping: true,
      style: "decimal",
      minimumFractionDigits: 1,
      maximumFractionDigits: 1,
    })
      .format(num)
      .replace(/,/g, " ")
  }

  @bind
  formatCurrency(num: number): string {
    return num.toFixed(2)
  }

  @bind
  formatStorageSize(num: number, format: string = "abbr-reduce") {
    const getUnit = (unit: string) => {
      if (format.indexOf("abbr") == 0) {
        return this.translate("storage_size_units.abbr." + unit)
      } else {
        return this.translate("storage_size_units.full." + unit)
      }
    }

    if (format == "abbr-reduce" || format == "full-reduce") {
      let stepper = num
      let steps = 0

      while (stepper > 1024) {
        stepper = stepper / 1024
        steps++
      }

      const suffix = (() => {
        switch (steps) {
          case 0:
            return getUnit("bytes")
          case 1:
            return getUnit("kilobytes")
          case 2:
            return getUnit("megabytes")
          case 3:
            return getUnit("gigabytes")
          case 4:
            return getUnit("terabytes")
          case 5:
            return getUnit("petabytes")
          default:
            return "?"
        }
      })()

      return (
        new Intl.NumberFormat(this.langCode, {
          maximumFractionDigits: 0,
        }).format(stepper) + suffix
      )
    } else {
      return (
        new Intl.NumberFormat(this.langCode).format(num) +
        " " +
        this.translate("storage_size_units.abbr.bytes")
      )
    }
  }
}

const CHANGE_LANG = "Localize/CHANGE_LANG"
import {ActionsBundle, StateBundle, StateAction} from '../State'

export interface LocalizeState {
  lang: string
}
type ReducerFunc = (state: LocalizeState, action: StateAction) => LocalizeState

const reducers: {[key: string]: ReducerFunc} = {}
const initialState = {lang: "en"}


interface ChangeLangAction extends StateAction {
  lang: string
}
function changeLanguage (lang: string) {
  return {type: CHANGE_LANG, lang}
}
reducers[CHANGE_LANG] = (state, action: ChangeLangAction) => {
  return {
    lang: action.lang
  }
}











const actions = {
  changeLanguage,
}

export function reducer (state: LocalizeState = initialState, action: StateAction): LocalizeState {
  if (reducers[action.type]) {
    const newState = reducers[action.type](state, action)
    return newState
  } else {
    return state
  }
}

const stateBundle: StateBundle<LocalizeState> = {
  name: "localize",
  initial: initialState,
  constants: {
    CHANGE_LANG,
  },
  reducer,
  actions,
}

export type LocalizeActions = ActionsBundle<typeof actions>
export default stateBundle

// Note Copyright where applicable
// Place test images inside this directory.

const images = {
  applecore: {
    src: "applecore.jpg",
    url: "/applecore.jpg",
    width: 2000,
    height: 2500,
    // focus: [0.75, 0.3], // optional
  },
}

const files = {}

export { images, files }

import { MenuLinkData, MarkdownBlockData } from "./FlexibleContent"
import { ImgData } from "./ImgData"
import { images } from "../cms-media"
import { WebMetadataData, RootViewData } from "./WebMetadataData"
import { GlobalData, GlobalContent } from "./GlobalData"

export interface HomePageData {
  metadata: WebMetadataData
  title: string
}

export const HomePageContent: HomePageData = {
  title: "Applecore",
  metadata: {
    seo_title: "",
    seo_description: "",
    seo_image: null,
  },
}

export interface HomePageViewData extends GlobalData, RootViewData {
  content: HomePageData
}

export const HomePageViewContent: HomePageViewData = Object.assign(
  {
    lang: "en",
    lang_urls: { en: "/" },
    root_domain: "http://localhost:3000",
    current_url: "http://localhost:3000",
    content: HomePageContent,
  },
  GlobalContent
)

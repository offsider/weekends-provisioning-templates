import { ImgData } from "./ImgData"

export interface MenuLinkData {
  title: string
  url: string
}

export interface FlexibleBlockData {
  type: string
  data: any
}

export interface MarkdownBlockData extends FlexibleBlockData {
  type: "MarkdownBlock"
  data: string
  html?: string
}

import { ImgData } from "./ImgData"

export interface WebMetadataData {
  seo_title?: string
  seo_description?: string
  seo_image?: ImgData
}

export interface RootViewData {
  lang: string
  lang_urls: { [key: string]: string }
  root_domain: string
  current_url: string
  as_pdf?: boolean
  pdf_root_url?: string
}

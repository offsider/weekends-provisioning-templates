import { MenuLinkData } from "./FlexibleContent"

export interface GlobalInformationData {
  main_navigation_links: MenuLinkData[]
}

export interface SiteConfigurationData {
  analytics_google_site_id?: string
}

export interface GlobalData {
  global: GlobalInformationData
  config: SiteConfigurationData
}

export const GlobalContent: GlobalData = {
  global: {
    main_navigation_links: [
    ],
  },
  config: {
    analytics_google_site_id: "UWUUWU",
  },
}

export interface ImgData {
  src: string
  url?: string
  focus?: number[]
  title?: string
  longdesc?: string
  filesize?: number
  width: number
  height: number
}

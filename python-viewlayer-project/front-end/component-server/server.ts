import { UniversalComponentServer } from "universal-component-server"

import * as express from "express"

import ucsConfig from "./config"
import {
  viewportMeta,
  nojsScript,
  vh,
} from "../headScripts"

const projectName = "AP"

const ucsServer = new UniversalComponentServer(ucsConfig, {
  scripts: ["/" + projectName + "Components.client.js"],
  stylesheets: ["/assets/" + projectName + "Main.css"],
  head: [viewportMeta, nojsScript, vh].join("\n"),
})

if (process.argv.indexOf("static") != -1) {
  // const memwatch: any = require('memwatch-next')
  // memwatch.on('leak', function(info: any) {
  //   console.log('leak', info)
  // })

  // let hd = new memwatch.HeapDiff()

  // setInterval(function () {
  //   const diff = hd.end()
  //   console.log('diff', diff)
  //   hd = new memwatch.HeapDiff()
  // }, 5000)

  ucsServer.renderStatic(__dirname)
} else {
  ucsServer.configApp((app) => {
    app.use("/", express.static(__dirname))
  })
  ucsServer.runServer()
}

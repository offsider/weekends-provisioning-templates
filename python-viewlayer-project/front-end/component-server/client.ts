import { UniversalComponentClient } from "universal-component-server"
import "core-js/es6/array"
import "core-js/es6/promise"
import "core-js/es6/object"

import ucsConfig from "./config"
const ucsClient = new UniversalComponentClient(ucsConfig)

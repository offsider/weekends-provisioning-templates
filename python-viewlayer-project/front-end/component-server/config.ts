import { UniversalComponentConfig } from "universal-component-server"
import { componentServerGetUrl } from "../Routes"

const context = (require as any).context("../ui", true, /\.(js|ts|tsx)$/)
const ucsConfig = new UniversalComponentConfig(context)
const OnDemandResizer: any = require("on-demand-resizer")

const resizer =
  OnDemandResizer &&
  OnDemandResizer({
    sourcePath:
      process.env.RESIZER_SOURCE ||
      process.env.COMPONENT_MEDIA_SRC ||
      __dirname + "/../../front-end/cms-media",
    destPath: process.env.RESIZER_DEST || __dirname + "/media",
    urlBase: "/media",
    imageMagick: true,
    workers: 1,
    defaultQuality: 80,
  })

import AppRoot from "../platform/AppContext/AppContext"

ucsConfig.addComponentRunner({
  name: "react-components",
  matcher: "./**/*.react.tsx",
  getTestData: path => {
    const keys = context.keys()
    const testPaths = [
      path.replace(".react.tsx", ".data.tsx"),
      path.replace(".react.tsx", ".data.ts"),
    ]

    for (let i = 0; i < testPaths.length; i++) {
      let dataPath = testPaths[i]
      if (keys.indexOf(dataPath) != -1) {
        return context(dataPath)
      }
    }
    return { default: {} }
  },
  renderServer: (componentModule, data) => {
    const ReactDOMServer = require("react-dom/server")
    const React = require("react")

    return ReactDOMServer.renderToString(
      React.createElement(
        AppRoot,
        {
          lang: "en",
          getUrl: componentServerGetUrl,
          resizer,
        },
        React.createElement(componentModule.default, data)
      )
    )
  },
  renderClient: (container, componentModule, data, path) => {
    try {
      const clientModule = context(path.replace("react.tsx", "test.ts"))
      clientModule.default(container)
    } catch (e) {
      console.log("No client test code for this module", e)
    }
    return null
  },
})

export default ucsConfig

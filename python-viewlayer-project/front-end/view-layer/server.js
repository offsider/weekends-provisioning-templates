const viewlayer = require("view-layer")
const _ = require("lodash")
const path = require("path")
const fs = require("fs")
const winston = require("winston")
const render = require("./render.js").default

Object.assign = Object.assign || require("object.assign")

const logger = winston.createLogger({
  level: "info",
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
      filename: __dirname + "/error.log",
      level: "error",
    }),
    new winston.transports.File({ filename: __dirname + "/all.log" }),
  ],
})

const app = viewlayer({
  bodyParser: { limit: "1024kb" },
})

var routeMap = {
  "/home": require("../ui/views/HomePage"),
  "/generic": require("../ui/views/GenericPage"),
}

_.forEach(routeMap, (component, route) => {
  const context = {}

  if (component.default) {
    component = component.default
  }
  app.route(route, render(component), context)
})

var port = process.env.VIEW_LAYER_PORT
  ? parseInt(process.env.VIEW_LAYER_PORT)
  : 4000

var hostname =
  process.env.VIEW_LAYER_ENV === "production" ? "127.0.0.1" : undefined

if (process.env.VIEW_LAYER_ENV != "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    })
  )
}

app.listen(port, hostname, undefined, function() {
  logger.info("View Layer listening to " + hostname + " on port " + port)
})

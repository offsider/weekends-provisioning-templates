const React = require("react"),
  ReactDOM = require("react-dom/server"),
  OnDemandResizer = require("on-demand-resizer"),
  _ = require("lodash")

const fs = require("fs")
const winston = require("winston")

import AppRoot from "../platform/AppContext"
import { viewLayerGetUrlEn } from "../Routes"
let world, resizer, buildVersion
const rootTemplate = require("pug-loader!./wrapper.pug")
import { viewportMeta, nojsScript, vh } from "../headScripts"

fs.readFile(__dirname + "/build_version", function(err, file) {
  if (!err) {
    buildVersion = file.toString().trim()
    winston.info("Cache Version is '" + buildVersion + "'")
  } else {
    buildVersion = "develop"
    winston.info("Cache Version is 'development'")
  }
})

const LOCALE_ROUTERS = {
  en: viewLayerGetUrlEn,
}

export default function(templateComponent) {
  var opts = {}

  if (!world) {
    world = {}
    resizer = OnDemandResizer({
      sourcePath: process.env.RESIZER_SOURCE || "/data/public/media",
      destPath: process.env.RESIZER_DEST || "/data/public/media/cache",
      urlBase: "/media/cache",
      imageMagick: true,
      workers: 3,
      defaultQuality: 80,
    })
    const loadSvgMedia = function(path) {
      const base =
        process.env.RESIZER_SOURCE || __dirname + "/../../.tmp/uploads"
      const fullPath = base + "/" + path
      return fs.readFileSync(fullPath, "utf8")
    }

    world.loadSvgMedia = loadSvgMedia
    // world.routes = new r.RouteTable(r.defaultRouteTable)
  }

  opts.world = world

  return function(args) {
    var finalArgs = _.assign({}, opts, args)
    var pageHTML = ReactDOM.renderToStaticMarkup(
      React.createElement(
        AppRoot,
        {
          resizer,
          getUrl: LOCALE_ROUTERS[finalArgs.lang],
          session: args.session || {},
          lang_urls: finalArgs.lang_urls,
          pdf_render: finalArgs.pdf_render,
          lang: finalArgs.lang,
        },
        React.createElement(templateComponent, finalArgs)
      )
    )

    var pageTitle =
      (finalArgs.page &&
        finalArgs.page.metadata &&
        finalArgs.page.metadata.seo_title) ||
      finalArgs.page_title ||
      (finalArgs.page && finalArgs.page.title) ||
      "Untitled"

    var pageDescription =
      (finalArgs.page &&
        finalArgs.page.metadata &&
        finalArgs.page.metadata.seo_description) ||
      finalArgs.page_description ||
      (finalArgs.page && finalArgs.page.page_description)

    var render
    render = rootTemplate(
      _.assign(finalArgs, {
        VIEW_LAYER_ENV: process.env.VIEW_LAYER_ENV,
        resize: resizer.resize,
        buildVersion,
        pageTitle,
        pageDescription,
        pageHTML,

        // Head Scripts
        viewportMeta,
        nojsScript,
        vh,
      })
    )
    return render
  }
}

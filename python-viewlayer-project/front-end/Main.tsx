import "core-js/es6/array"
import "core-js/es6/promise"
import "core-js/es6/object"

import InitClient from "./InitClient"

document.addEventListener("DOMContentLoaded", () => {
  InitClient(document.body)
})

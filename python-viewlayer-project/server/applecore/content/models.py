from django.db import models
from jc.models import Media, ObjectTranslation
from jc.fields import FlexibleContentField
from django.contrib.contenttypes.fields import GenericRelation
from jc.json_format import ContentJSONFormat
from django.utils import timezone
from django.utils.text import slugify

from applecore.cache import (
    trigger_cache_key_rebuild,
    trigger_full_cache_rebuild,
)


def get_model_registry():
    from applecore.jcadmin import jcadmin
    return jcadmin.object_model_registry


class WebMetaDataAbstract(models.Model, ContentJSONFormat):
    seo_title = models.CharField(max_length=256, blank=True, null=True,
                                 default="")
    seo_description = models.TextField(blank=True, null=True, default="")
    seo_image = models.ForeignKey(Media, models.PROTECT, related_name="+", blank=True, null=True)

    class Meta:
        abstract = True

    def to_json(self, summary=False):
        json = super(WebMetaDataAbstract, self).to_json(summary)
        if not summary:
            json["metadata"] = {
                "seo_title": self.seo_title,
                "seo_description": self.seo_description,
                "seo_image": json["seo_image"],
            }

        del json["seo_title"]
        del json["seo_description"]
        del json["seo_image"]
        return json


class MenuLink(models.Model):
    title = models.CharField(max_length=256)
    url = models.URLField()

    jc_group_summary = {
        "line_2": "{{title}} ({{url.scope}}:{{url.link}})",
    }

    class Meta:
        abstract = True


class GlobalInformation(models.Model, ContentJSONFormat):
    translations = GenericRelation(ObjectTranslation)

    main_navigation_links = FlexibleContentField(
        markdown=False, default=[], allowed_blocks=[MenuLink],
        blank=True, null=True
    )

    json_ignore = ['translations']

    def save(self, *args, **kwargs):
        result = super(self.__class__, self).save(*args, **kwargs)
        trigger_full_cache_rebuild()
        return result


class HomePage(WebMetaDataAbstract, ContentJSONFormat):
    translations = GenericRelation(ObjectTranslation)

    class HeroSlide(models.Model):
        image = models.ForeignKey(Media, models.PROTECT, related_name="+")
        caption = models.TextField(blank=True, null=True)
        link = models.URLField(blank=True, null=True)

        jc_group_summary = {
            'thumbnail': 'image',
            'line_1': '{{link.scope}}:{{link.link}}',
            'line_2': '{{caption}}',
        }

        class Meta:
            abstract = True

    title = models.CharField(max_length=1024, default="Hello")

    json_ignore = ['translations']

    def get_cache_paths(self):
        return [
            "/",
        ]

    def save(self, *args, **kwargs):
        result = super(self.__class__, self).save(*args, **kwargs)
        trigger_cache_key_rebuild(self.get_cache_paths())
        return result


class SiteConfiguration(models.Model, ContentJSONFormat):
    translations = GenericRelation(ObjectTranslation)
    class NotificationEmail(models.Model):
        email = models.EmailField()

        jc_group_summary = {
            'line_1': '{{email}}',
        }

        class Meta:
            abstract = True

    class Redirect(models.Model):
        in_url = models.URLField()
        out_url = models.URLField()
        permanent = models.BooleanField(default=False)

        jc_group_summary = {
            'line_2': '{{in_url.link}} -> {{out_url.link}} {{#permanent}}(Permanent){{/permanent}}',
        }

        class Meta:
            abstract = True

    json_ignore = ['translations', 'seo_redirects']

    # notifications_email = FlexibleContentField(
    #     markdown=False, default=[], allowed_blocks=[NotificationEmail],
    #     blank=True, null=True
    # )

    seo_redirects = FlexibleContentField(
        markdown=False, default=[], allowed_blocks=[Redirect],
        blank=True, null=True
    )

    analytics_google_site_id = models.CharField(max_length=256, blank=True)


class GenericPage(WebMetaDataAbstract, ContentJSONFormat):
    translations = GenericRelation(ObjectTranslation)

    title = models.CharField(max_length=512)
    slug = models.SlugField(max_length=512)

    main_content = FlexibleContentField(
        model_registry=get_model_registry,
        blank=True, null=True,
        markdown=True, default=[], allowed_blocks=[ ]
    )

    json_summary_ignore = ['main_content']
    json_ignore = ['translations']

    def get_cache_paths(self):
        return [
            "/{0}/".format(self.slug)
        ]

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        result = super().save(*args, **kwargs)
        trigger_cache_key_rebuild(self.get_cache_paths())
        return result

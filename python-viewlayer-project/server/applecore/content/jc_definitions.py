from applecore.content.models import (
    HomePage,

    GenericPage,
    GlobalInformation,
    SiteConfiguration,

)

from jc.admin import ContentTemplate, Tab, Field

default_tabs_with_metadata = [{
    "name": "Content",
    "remainder": True,
}, {
    "name": "Metadata",
    "fields": [
        "seo_title", "seo_description", "seo_image",
    ],
}]


class HomePageTemplate(ContentTemplate):
    model = HomePage
    label = "Home Page"
    slug = "home-page"
    singular = True
    tabs = default_tabs_with_metadata


class GenericPageTemplate(ContentTemplate):
    model = GenericPage
    label = "Generic Pages"
    slug = "generic-pages"
    index_fields = ("title", "slug",)
    tabs = default_tabs_with_metadata
    field_options = {
        "slug": {"before": "/", "required": False},
    }
class GlobalInformationTemplate(ContentTemplate):
    model = GlobalInformation
    label = "Global Information"
    slug = "global-information"
    singular = True

class SiteConfigurationTemplate(ContentTemplate):
    model = SiteConfiguration
    label = "Site Configuration"
    slug = "site-configuration"
    singular = True

    tabs = [{
        "name": "Notifications",
        "fields": [
        ],
    }, {
        "name": "SEO",
        "fields": [
            "seo_redirects",
        ],
    }, {
        "name": "Analytics",
        "fields": [
            "analytics_google_site_id",
        ],
    }]

from django.db import models
from jc.models import Media
from jc.fields import FlexibleContentField

def get_model_registry():
    from applecore.jcadmin import jcadmin
    return jcadmin.object_model_registry

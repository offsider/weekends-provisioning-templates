from applecore.view_layer import render_payload
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.conf import settings
from jc.models import ObjectTranslation
from applecore.view_helpers import get_lang_urls, translate_to_locale


from jc.translation import apply_translation
from django.urls import reverse
from copy import copy


from applecore.content.models import (
    HomePage,
    GenericPage,
)


def home(request, lang=settings.LANGUAGE_CODE):
    lang_urls = get_lang_urls(request)

    home_page = HomePage.objects.first() or HomePage()
    translate_to_locale(home_page, lang)

    return render_payload(request, '/home', {
        "lang": lang,
        "page_title": "Home",
        "lang_urls": lang_urls,
        "content": home_page.to_json(),
    })


def get_localized_or_404(Model, slug, lang):
    if lang != settings.LANGUAGE_CODE:
        try:
            instance = Model.objects.get(
                translations__localized_slug=slug,
                translations__lang=lang
            )
        except Model.DoesNotExist:
            try:
                instance = Model.objects.get(slug=slug)
            except Model.DoesNotExist:
                raise Http404("{0} not found".format(Model))
    else:
        try:
            instance = Model.objects.get(slug=slug)
        except Model.DoesNotExist:
            raise Http404("{0} not found".format(Model))

    return instance


def objects(request, lang=settings.LANGUAGE_CODE):
    lang_urls = get_lang_urls(request)

    objects_page = ObjectsPage.objects.first() or ObjectsPage()
    translate_to_locale(objects_page, lang)

    objects = Object.objects.all()
    for o in objects:
        translate_to_locale(o, lang)

    content = objects_page.to_json()
    content["objects"] = [o.to_json_summary() for o in objects]

    return render_payload(request, '/objects', {
        "lang": lang,
        "page_title": "Objects",
        "lang_urls": lang_urls,
        "content": content,
    })


def generic(request, slug, lang=settings.LANGUAGE_CODE):
    generic = get_localized_or_404(GenericPage, slug, lang)
    lang_urls = get_lang_urls(request)
    translate_to_locale(generic, lang)
    content_js = generic.to_json()
    content_js["is_generic"] = True

    return render_payload(request, '/generic', {
        "lang": lang,
        "page_title": generic.title,
        "lang_urls": lang_urls,
        "content": content_js,
    })

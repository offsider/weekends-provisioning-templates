VIEW_LAYER_URL = "http://view-layer:4000"
STATIC_URL = "/assets/"

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
ALLOWED_HOSTS = ['*']

CACHE_ROOT_URL = "http://web:8000"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': "postgres",
        'USER': "postgres",
        'HOST': 'db',
        'PORT': 5432,
    }
}

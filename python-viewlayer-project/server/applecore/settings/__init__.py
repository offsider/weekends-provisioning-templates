from .common import *
from .dev import *

try:
    from .local import *
except ImportError:
    pass

try:
    from local_settings import *
except ImportError:
    pass

if "DJANGO_ENV_CONFIG" in os.environ:
    from .env import *

import os

# SECRET_KEY = os.environ["DJANGO_SECRET_KEY"]
MEDIA_ROOT = "/data/public/media"
STATIC_ROOT = "/data/public/assets"

if "APP_DEBUG" in os.environ:
    DEBUG = True
    TEMPLATE_DEBUG = False
else:
    DEBUG = False
    TEMPLATE_DEBUG = False

if "APP_ALLOWED_HOSTS" in os.environ:
    ALLOWED_HOSTS = os.environ["DJANGO_ALLOWED_HOSTS"].split("|")

if "MAILGUN_SERVER_NAME" in os.environ:
    EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
    ANYMAIL = {
        "MAILGUN_SENDER_DOMAIN": os.environ["MAILGUN_SERVER_NAME"],
        "MAILGUN_API_KEY": os.environ["MAILGUN_ACCESS_KEY"],
    }

if "APP_SECRET_KEY" in os.environ:
    SECRET_KEY = os.environ["APP_SECRET_KEY"]

if "DB_PASSWORD" in os.environ:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': "postgres",
            'USER': "postgres",
            'PASSWORD': os.environ["DB_PASSWORD"],
            'HOST': 'db',
            'PORT': 5432,
        }
    }

if "APP_CACHE_ROOT_URL" in os.environ:
    CACHE_ROOT_URL = os.environ["APP_CACHE_ROOT_URL"]

if "DJANGO_FROM_EMAIL" in os.environ:
    DEFAULT_FROM_EMAIL = os.environ["DJANGO_FROM_EMAIL"]

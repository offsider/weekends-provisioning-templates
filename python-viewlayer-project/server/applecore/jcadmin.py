from jc.admin import JCAdminSite, JCAdminGroup
from django.contrib import admin


from applecore.content.jc_definitions import (
    HomePageTemplate,
    GenericPageTemplate,
    GlobalInformationTemplate,
    SiteConfigurationTemplate,
)



jcadmin = JCAdminSite(structure=[
    HomePageTemplate,
    GenericPageTemplate,
    GlobalInformationTemplate,
    SiteConfigurationTemplate,
])

admin.site.site_header = "Weekends Template Website"
admin.site.site_title = "Weekends Template"

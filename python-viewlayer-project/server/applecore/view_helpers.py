from copy import copy
from jc.models import ObjectTranslation
from jc.translation import apply_translation
from applecore.jcadmin import jcadmin

from django.conf import settings
from django.urls import reverse


# NOTE: this MUST be called before applying a translation mask, otherwise
# the base language url will not be correct.
def get_lang_urls(request, slug_object=None):
    viewname = request.resolver_match.url_name.split('/')
    current_lang = viewname.pop(0)
    viewkwargs = request.resolver_match.kwargs
    langs = [settings.LANGUAGE_CODE] + settings.JC_LANGUAGE_TRANSLATIONS
    urls = {}

    if slug_object:
        slugs = {}
        slugs[settings.LANGUAGE_CODE] = slug_object.slug
        translations = slug_object.translations.all()
        for t in translations:
            slugs[t.lang] = t.localized_slug
    else:
        slugs = None

    for lang in langs:
        langviewname = '/'.join([lang] + viewname)
        langkwargs = copy(viewkwargs)
        if 'lang' in langkwargs:
            del langkwargs["lang"]

        if slugs:
            if lang in slugs:
                langkwargs["slug"] = slugs[lang]
            else:
                langkwargs["slug"] = slugs[settings.LANGUAGE_CODE]

        urls[lang] = reverse(langviewname, kwargs=langkwargs)

    return urls


def translate_to_locale(obj, lang):
    try:
        trans = obj.translations.get(lang=lang)
        apply_translation(jcadmin, obj, trans.translation_mask)
    except ObjectTranslation.DoesNotExist:
        return


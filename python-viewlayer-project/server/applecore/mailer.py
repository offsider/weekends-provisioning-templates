from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template, render_to_string, TemplateDoesNotExist
from django.template import Context, Template
from django.utils.html import strip_tags
from django.utils.module_loading import import_string
from django.conf import settings
import markdown
import logging
import bleach
import json
from django.utils.safestring import mark_safe
logger = logging.getLogger(__name__)
from django.contrib.sites.models import Site

def send_template_email(subject, template, context_dict, from_email,
                        recipient_list, html_template='', attachments=[],
                        *args, **kwargs):
    """
    A wrapper for django.core.mail.send_mail() that provides it
    with a template and context to render for the message.

    pre:
        isinstance(subject, (str, unicode))
        isinstance(template, (str, unicode))
        isinstance(context_dict, dict)
        isinstance(from_email, (str, unicode))
        isinstance(recipient_list, (list, tuple))
    """
    context_dict["site"] = Site.objects.get_current()
    text_message = render_to_string(template, context_dict)

    for recipient in recipient_list:
        email = EmailMultiAlternatives(subject, text_message, from_email,
                                       [recipient])
        if html_template:
            try:
                html_message = render_to_string(html_template, context_dict)
            except TemplateDoesNotExist:
                pass
            else:
                email.attach_alternative(html_message, "text/html")
        for attachment in attachments:
            email.attach_file(attachment)
        email.send()
    logger.info("Sent Email to {0}".format(', '.join(recipient_list)))

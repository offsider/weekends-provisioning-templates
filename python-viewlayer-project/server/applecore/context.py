from django.middleware import csrf
from django.http import HttpResponse
import json
from django.conf import settings
from django.views.decorators.cache import never_cache
from applecore.view_helpers import translate_to_locale
from applecore.content.models import GlobalInformation, SiteConfiguration
from decimal import Decimal

def build(request, lang="en", attach_session=False):
    protocol = request.is_secure() and "https://" or "http://"

    global_info = GlobalInformation.objects.first() or GlobalInformation()
    translate_to_locale(global_info, lang)
    global_json = global_info.to_json()

    site_config = SiteConfiguration.objects.first() or SiteConfiguration()
    translate_to_locale(site_config, lang)
    config_json = site_config.to_json()


    return {
        "session": {},
        "lang": lang,
        "root_domain": "{0}{1}".format(protocol, request.get_host()),
        "current_url": request.build_absolute_uri(),
        "global": global_json,
        "config": config_json,
    }


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

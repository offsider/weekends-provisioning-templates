"""smc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.http import HttpResponse
from django.contrib import admin
from django.conf.urls.static import static

from django.views.decorators.cache import never_cache
from django.urls import path
from django.conf import settings
from applecore.jcadmin import jcadmin

en = {"lang": "en"}

from applecore.content import views as content_views

urlpatterns = ([
    path('', content_views.home, en, name="en/home"),
])

urlpatterns = urlpatterns + ([
    url(r'^admin/jc/', jcadmin.urls),
    url(r'^admin/', admin.site.urls),
    path('<slug:slug>/', content_views.generic, en, name="en/generic"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT))


handler404 = "applecore.view_layer.handler404"
handler500 = "applecore.view_layer.handler500"

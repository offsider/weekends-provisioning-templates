from django.core.management.base import BaseCommand, CommandError
import time


class Command(BaseCommand):
    help = 'Clear the Cache'

    def handle(self, *args, **options):
        from snapshot.cache import trigger_full_cache_rebuild
        results = trigger_full_cache_rebuild(True)
        print("Cache Cleared.")

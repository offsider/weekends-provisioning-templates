from django import forms
from django.forms.forms import pretty_name
from django.forms.widgets import PasswordInput
from django.template import Context, Template
from django.conf import settings
from django.template.loader import get_template, render_to_string
from django.utils.timezone import now


class JSONForm(object):
    def to_json(self):
        fields = {}
        errors = {}
        field_order = []

        if "__all__" in self.errors:
            errors["__all__"] = self.errors["__all__"]
        else:
            errors["__all__"] = []

        for key, field in self.fields.items():
            name = key
            if self.prefix:
                name = "{0}-{1}".format(self.prefix, key)

            field_order.append(key)
            data = self[key].value()

            if isinstance(field.widget, PasswordInput):
                data = None

            fields[key] = {
                "label": field.label or pretty_name(key),
                "type": field.__class__.__name__,
                "widget": field.widget.__class__.__name__,
                "initial": field.initial,
                "data": data,
                "required": field.required,
                "errors": [],
                "name": name,
            }

            if key in self.errors:
                fields[key]["errors"] = [l for l in self.errors[key]]
                # errors[key] = [l for l in self.errors[key]]

            if self.initial and key in self.initial:
                fields[key]["initial"] = self.initial[key]

            if hasattr(field, "choices"):
                fields[key]["choices"] = list(field.choices)

        return {
            "fields": fields, "errors": errors,
            "field_order": field_order, "non_field_errors": errors["__all__"]
        }

        return self.view_layer_json()


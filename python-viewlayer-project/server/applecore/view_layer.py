import json
from django.http import (HttpResponseRedirect, Http404,
                         HttpResponse, HttpResponseServerError)

from django.urls import reverse
from django.shortcuts import render
from django.conf import settings
from django.template import RequestContext
from decimal import Decimal
import requests
from uuid import UUID
from io import BytesIO
from datetime import datetime, time, date
from django.utils.functional import Promise

from applecore.context import build as build_context


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Polygon):
            return obj.tuple
        if isinstance(obj, UUID):
            return str(obj)
        if isinstance(obj, Decimal):
            return float(obj)
        if isinstance(obj, Promise):
            return unicode(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        if isinstance(obj, time):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)

cached500 = None


def render_email(path, payload):
    url = "{0}{1}".format(settings.VIEW_LAYER_URL, path)

    r = requests.put(
        url,
        data=json.dumps(payload, cls=DecimalEncoder),
        headers={'content-type': 'application/json'}
    )

    return r.text


def render_payload_to_html(request, path, payload, attach_session=False):
    global cached500
    url = "{0}{1}".format(settings.VIEW_LAYER_URL, path)

    final_payload = build_context(
        request, lang=payload.get("lang", "en"),
        attach_session=attach_session
    )
    final_payload.update(payload)

    if request.GET.get("investigate_json", None) == "true" and settings.DEBUG:
        return HttpResponse(
            json.dumps(final_payload, cls=DecimalEncoder),
            content_type="application/json"
        )

    if not cached500:
        url500 = "{0}{1}".format(settings.VIEW_LAYER_URL, "/generic")
        payload500 = build_context(request, False)
        payload500["page_title"] = "Server Error"
        payload500["lang"] = "en"
        payload500["lang_urls"] = {"en": "/"}
        payload500["content"] = {
            "title": "Server Error",
            "body": [{
                "type": "MarkdownBlock",
                "data": """There was an error on the server. We have been notified and will investigate shortly."""
            }],
        },

        req = requests.put(
            url500,
            data=json.dumps(payload500, cls=DecimalEncoder),
            headers={'content-type': 'application/json'}
        )
        cached500 = req.text

    r = requests.put(
        url,
        data=json.dumps(final_payload, cls=DecimalEncoder),
        headers={'content-type': 'application/json'}
    )

    return r.text
    if int(r.status_code) >= 500:
        raise Exception(r.text)
    else:
        return r.text


def render_payload(request, path, payload, attach_session=False, response_code=200):
    payload["pdf_render"] = False
    response_html = render_payload_to_html(request, path, payload, attach_session)
    return HttpResponse(response_html, status=response_code)


def handler500(request):
    global cached500
    return HttpResponse(cached500, status=500)


def handler404(request, exception):
    return render_payload(request, "/generic", {
        "lang": "en",
        "page_title": "Page Not Found",
        "lang_urls": { "en": "/", },
        "content": {
            "title": "Page Not Found",
            "body": [{
                "type": "MarkdownBlock",
                "data": """The requested page could not be found."""
            }],
        },
    }, response_code=404)

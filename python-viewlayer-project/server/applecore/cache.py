from django.core.cache import cache
import time
from django.utils import timezone
from django.conf import settings
from multiprocessing import Pool, cpu_count

import requests


def trigger_full_cache_rebuild(wait_for=False):
    from applecore.content.models import (
        HomePage,
        GenericPage,
    )

    paths = []
    for Model in [
        HomePage,
        GenericPage,
    ]:
        for c in Model.objects.all():
            paths += c.get_cache_paths()
        return trigger_cache_key_rebuild(paths, wait_for)



def trigger_cache_key_rebuild(paths, wait_for=False):
    uniq_paths = set(paths)
    results = []
    with Pool(2) as pool:
        for path in uniq_paths:
            url = '{0}{1}'.format(settings.CACHE_ROOT_URL, path)
            use_auth = 'qa.' in settings.CACHE_ROOT_URL
            results.append(
                pool.apply_async(async_func, [url, use_auth])
            )
        if wait_for:
            [result.wait() for result in results]
    return results

def async_func(url, use_auth):
    print("hitting -> {0}".format(url))
    time.sleep(2)
    if use_auth:
        response = requests.get(url, auth=('applecore', os.environ.get("WEB_HTACCESS_PWD", "")), params={"nocache": "true"})
    else:
        response = requests.get(url, params={"nocache": "true"})
    print(("finished -> {0}".format(url), response))

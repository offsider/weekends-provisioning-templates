# Docker Build

```
vagrant up
vagrant ssh

cd /vagrant
docker-compose up
```

Access the server at:
`localhost:8000`

Access the component server at:
`localhost:3000`

## Manage Django with:

```
vagrant ssh
cd /vagrant
docker-compose run web python manage.py migrate
```

## Run Anathema with:

Faster, local development:

```
yarn install
yarn anathema
```

Slower, but using Docker and requires no system node:

```
vagrant ssh
cd /vagrant
docker-compose run view-layer yarn anathema
```

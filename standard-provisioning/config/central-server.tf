provider "linode" {
  token = var.linode_token
}

resource "linode_instance" "central-server" {
	image = "linode/fedora30"
	label = "applecore-template"
	group = "ApplecoreTemplate"
	region = "ap-south"
	type = "g6-standard-1"
	authorized_keys = [ var.ssh_key ]
	root_pass = var.root_password
}

output "site_ip" {
	value = linode_instance.central-server.ip_address
}

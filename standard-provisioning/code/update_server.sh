#!/bin/bash

set -o allexport
source ./SECRETS.env
set +o allexport

ansible-playbook webserver.yml -v -i hosts -u root --private-key ./secrets/ROOT_KEY

fly -t ${CONCOURSE_USER} login --concourse-url https://${CONCOURSE_DOMAIN} -u ${CONCOURSE_USER} -p ${CONCOURSE_PASS}
fly -t ${CONCOURSE_USER} sync
fly -t ${CONCOURSE_USER} status
fly -t ${CONCOURSE_USER} set-pipeline \
	--pipeline ${CONCOURSE_PIPELINE} \
	--config build-pipeline.yml \
	--var "server-ip=$(cat ./SERVER_IP)" \
	--var "private-repo-key=$(cat ./secrets/BITBUCKET_KEY)" \
	--var "private-concourse-key=$(cat ./secrets/CONCOURSE_KEY)" \
	--var "private-deploy-key=$(cat ./secrets/DEPLOY_KEY)"
fly -t ${CONCOURSE_USER} unpause-pipeline --pipeline ${CONCOURSE_PIPELINE}

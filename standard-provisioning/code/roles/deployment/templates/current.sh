#!/bin/bash

set -o allexport
source /home/{{item.name}}/config/app.env
set +o allexport

cd /home/{{item.name}}/webserver/current
docker-compose "$@"

#!/bin/bash

set -o allexport
source /home/{{item.name}}/config/app.env
set +o allexport

cd /home/{{item.name}}/webserver/$1
docker-compose "${@:2}"

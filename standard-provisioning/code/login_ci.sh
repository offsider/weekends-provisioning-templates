#!/bin/bash

set -o allexport
source ./SECRETS.env
set +o allexport

fly -t ${CONCOURSE_USER} login --concourse-url http://${CONCOURSE_DOMAIN} -u ${CONCOURSE_USER} -p ${CONCOURSE_PASS}
fly -t ${CONCOURSE_USER} sync
fly -t ${CONCOURSE_USER} status

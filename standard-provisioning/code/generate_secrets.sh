#!/bin/bash

mkdir -p ./secrets
ssh-keygen -t rsa -b 4096 -C "admin@applecore.com" -f ./secrets/ROOT_KEY -q -N ""
ssh-keygen -t rsa -b 4096 -C "admin@applecore.com" -f ./secrets/CONCOURSE_KEY -q -N ""
ssh-keygen -t rsa -b 4096 -C "admin@applecore.com" -f ./secrets/BITBUCKET_KEY -q -N ""
ssh-keygen -t rsa -b 4096 -C "admin@applecore.com" -f ./secrets/DEPLOY_KEY -q -N ""
openssl rand -hex 32 > ./secrets/ROOT_PASSWORD
openssl rand -hex 32 > ./secrets/QA_ENCRYPTION
openssl rand -hex 32 > ./secrets/PROD_ENCRYPTION
openssl rand -hex 16 > ./secrets/QA_PASSWORD
openssl rand -hex 16 > ./secrets/PROD_PASSWORD
openssl rand -hex 16 > ./secrets/CONCOURSE_ENCRYPTION
openssl rand -hex 16 > ./secrets/WEB_HTACCESS_PASSWORD
openssl rand -hex 16 > ./secrets/CONCOURSE_PASSWORD
openssl rand -hex 16 > ./secrets/SUDO_PASSWORD

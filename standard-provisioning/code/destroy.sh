#!/bin/bash

set -o allexport
source ./SECRETS.env
set +o allexport
terraform init
terraform destroy \
	-var="linode_token=${LINODE_API_KEY}" \
	-var="ssh_key=${LINODE_SSH_KEY}" \
	-var="root_password=${LINODE_ROOT_PASSWORD}"

variable "linode_token" {
	description = "Your APIv4 Access Token"
}
variable "root_password" {
	description = "Your Root Password"
}
variable "ssh_key" {
  description = "The local file location of the SSH key that will be transferred to each Linode."
}

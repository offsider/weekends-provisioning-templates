#!/bin/bash

set -o allexport
source ./SECRETS.env
set +o allexport
terraform init
terraform apply \
	-var="linode_token=${LINODE_API_KEY}" \
	-var="ssh_key=${LINODE_SSH_KEY}" \
	-var="root_password=${LINODE_ROOT_PASSWORD}"

echo "[webservers]" > hosts
terraform output site_ip >> hosts

terraform output site_ip > SERVER_IP
